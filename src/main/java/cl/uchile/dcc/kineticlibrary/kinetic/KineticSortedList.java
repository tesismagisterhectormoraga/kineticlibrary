package cl.uchile.dcc.kineticlibrary.kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static java.lang.Double.NEGATIVE_INFINITY;
import static java.lang.Double.POSITIVE_INFINITY;
import static java.lang.String.valueOf;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import cl.uchile.dcc.kineticlibrary.kinetic.comparators.DimensionKineticComparator;
import cl.uchile.dcc.kineticlibrary.kinetic.comparators.DimensionKineticComparatorWithDelta;
import cl.uchile.dcc.kineticlibrary.kinetic.events.SubEventSwap;
import cl.uchile.dcc.staticlibrary.primitives.Intersections;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;

/**
 *
 * @author hmoraga
 */
public class KineticSortedList extends ArrayList<Wrapper>{

    /**
     * me da el orden inicial
     */
    public KineticSortedList() {
        super();
    }

    /**
     *
     * @param objeto
     * @param dimension
     * @return
     */
    public boolean add(Object2D objeto, int dimension) {
        return this.addAll(objeto.getWrappers(dimension));
    }

    /**
     *
     * @param c
     * @return
     */
    @Override
    public boolean addAll(Collection<? extends Wrapper> c) {
        return super.addAll(c);
    }

    /**
     *
     * @param i
     * @param j
     */
    public synchronized void swap(int i, int j) {
        Wrapper aux = super.get(i);
        super.set(i, super.get(j));
        super.set(j, aux);
    }

    /**
     *
     * @param posi
     * @param posj
     * @param currentTime
     * @return
     */
    public List<SubEventSwap> calculateNeighborsEvents(int posi, int posj, double currentTime) {
        List<SubEventSwap> listaNuevosEventos = new ArrayList<>();
        double timeEvent;
        
        if (this.size() >= 3) {
            if ((posi == 0) && (posj == 1)) {
                Wrapper vecino = this.get(2);
                timeEvent = this.get(posj).getEventTime(vecino);
                if (timeEvent > currentTime) {
                    SubEventSwap subev = new SubEventSwap(timeEvent, posj, 2);
                    listaNuevosEventos.add(subev);
                }
            } else if ((posi == this.size() - 2) && (posj == this.size() - 1)) {
                Wrapper vecino = this.get(this.size() - 3);
                timeEvent = this.get(posi).getEventTime(vecino);
                if ( timeEvent > currentTime) {
                    SubEventSwap subev = new SubEventSwap(timeEvent, this.size() - 3, posi);
                    listaNuevosEventos.add(subev);
                }
            } else {
                Wrapper vecino1 = this.get((posi>0)?posi - 1:posi);
                Wrapper vecino2 = this.get((posj<this.size()-1)?posj + 1:posj);
                timeEvent = this.get(posi).getEventTime(vecino1);
                if ( timeEvent > currentTime) {
                    SubEventSwap subev1 = new SubEventSwap(timeEvent, posi - 1, posi);
                    listaNuevosEventos.add(subev1);
                }
                timeEvent = this.get(posj).getEventTime(vecino2);
                if (timeEvent > currentTime) {
                    SubEventSwap subev2 = new SubEventSwap(timeEvent, posj, posj + 1);
                    listaNuevosEventos.add(subev2);
                }
            }
        }

        return listaNuevosEventos;
    }

    /**
     *
     * @param pos
     * @return
     */
    /*public List<SubEventSwap> calculateNeighborsEvents(int pos) {
        List<SubEventSwap> listaNuevosEventos = new ArrayList<>();
        double timeEvent;
        
        if (this.size() >= 2) {
            if (pos == 0) {
                Wrapper vecino = this.get(1);
                timeEvent = this.get(pos).getEventTime(vecino);
                if ( timeEvent > 0) {
                    SubEventSwap subev = new SubEventSwap(timeEvent, pos, 1);
                    listaNuevosEventos.add(subev);
                }
            } else if (pos == this.size() - 1) {
                Wrapper vecino = this.get(this.size() - 2);
                timeEvent = this.get(pos).getEventTime(vecino);
                if ( timeEvent > 0) {
                    SubEventSwap subev = new SubEventSwap(timeEvent, this.size() - 2, pos);
                    listaNuevosEventos.add(subev);
                }
            } else {
                Wrapper vecino1 = this.get(pos - 1);
                Wrapper vecino2 = this.get(pos + 1);
                if (this.get(pos).getEventTime(vecino1) > 0) {
                    SubEventSwap subev1 = new SubEventSwap(this.get(pos).getEventTime(vecino1), pos - 1, pos);
                    listaNuevosEventos.add(subev1);
                }

                if (this.get(pos).getEventTime(vecino2) > 0) {
                    SubEventSwap subev2 = new SubEventSwap(this.get(pos).getEventTime(vecino2), pos, pos + 1);
                    listaNuevosEventos.add(subev2);
                }
            }
        }

        return listaNuevosEventos;
    }*/

    /**
     *
     * @param puntoCinetico
     * @return
     */
    @Override
    public boolean add(Wrapper puntoCinetico) {
        return super.add(puntoCinetico);
    }

    /**
     *
     * @param time
     * @param include if include time in event search
     * @return event list
     */
    public List<SubEventSwap> calculateSwapEvents(double time, boolean include) {
        List<SubEventSwap> listaEventos = new ArrayList<>();

        if (this.size() >= 2) {
            for (int i = 1; i < this.size(); i++) {
                double timeEvent = this.get(i - 1).getEventTime(this.get(i));

                if (include) {
                    if (timeEvent >= time && timeEvent != POSITIVE_INFINITY) {
                        listaEventos.add(new SubEventSwap(timeEvent, i - 1, i));
                    }
                } else {
                    if (timeEvent > time && timeEvent != POSITIVE_INFINITY) {
                        listaEventos.add(new SubEventSwap(timeEvent, i - 1, i));
                    }
                }
            }
            return listaEventos;
        } else {
            return new ArrayList<>();
        }
    }

    /**
     *
     * @param idInicio id dentro de la ksl (inclusive)
     * @param idFinal id dentro de la ksl (inclusive)
     * @param time
     * @param include if include time in event search
     * @return event list
     */
    public List<SubEventSwap> calculateSwapEvents(int idInicio, int idFinal, double time, boolean include) {
        List<SubEventSwap> listaEventos = new ArrayList<>();

        if (this.size() >= 2) {
            if (idInicio != 0) {
                double timeEvent = this.get(idInicio - 1).getEventTime(this.get(idInicio));
                SubEventSwap sev = new SubEventSwap(timeEvent, idInicio - 1, idInicio);
                listaEventos.add(sev);
            }

            if (idFinal != this.size() - 1) {
                double timeEvent = this.get(idFinal).getEventTime(this.get(idFinal + 1));
                SubEventSwap sev = new SubEventSwap(timeEvent, idFinal, idFinal + 1);
                listaEventos.add(sev);
            }

            return listaEventos;
        } else {
            return null;
        }
    }

    /**
     * Metodo que revisa si el certificado es válido.
     * <P>
     * Un certificado es válido si para todos los puntos p_i de la lista
     * cinetica, con valores x_i son estrictamente mayores.
     *
     * @param time tiempo. Un valor mayor que cero.
     * @return true si el certificado es verdadero o false en caso contrario.
     */
    public boolean certificateValidation(double time) {
        return this.certificateValidation(0, super.size(), time);
    }

    /**
     * Metodo que revisa si el certificado es válido.
     * <P>
     * Un certificado es válido si para todos los puntos p_i de la lista
     * cinetica, con valores x_i son estrictamente mayores.
     *
     * @param time tiempo. Un valor mayor que cero.
     * @param delta un tiempo, positivo, mucho menor que time
     * @return true si el certificado es verdadero o false en caso contrario.
     */
    public boolean certificateValidation(double time, double delta) {
        return this.certificateValidation(0, super.size(), time, delta);
    }
    
    /**
     * Metodo que revisa si el certificado es válido.
     * <P>
     * Un certificado es válido si para todos los puntos p_i de la lista
     * cinetica, con valores x_i son estrictamente mayores.
     *
     * @param idinicio id de la lista desde el cual revisar si el orden esta correcto.
     * @param idfin id de la lista hasta el cual revisar si el orden esta correcto.
     * @param time tiempo. Un valor mayor que cero.
     * @return true si el certificado es verdadero o false en caso contrario.
     */
    public boolean certificateValidation(int idinicio, int idfin, double time) {
        return this.certificateValidation(idinicio, idfin, time, 1E-7);
    }

    /**
     * Metodo que revisa si el certificado es válido.
     * <P>
     * Un certificado es válido si para todos los puntos p_i de la lista
     * cinetica, con valores x_i son estrictamente mayores.
     *
     * @param idinicio id de la lista desde el cual revisar si el orden esta correcto.
     * @param idfin id de la lista hasta el cual revisar si el orden esta correcto.
     * @param time tiempo. Un valor mayor que cero.
     * @param delta
     * @return true si el certificado es verdadero o false en caso contrario.
     */
    public boolean certificateValidation(int idinicio, int idfin, double time, double delta) {
        DimensionKineticComparator dkc = new DimensionKineticComparator(time);
        DimensionKineticComparatorWithDelta dkcwd = new DimensionKineticComparatorWithDelta(time, delta);
        
        if (super.size() > 1) {
            int num = 0;

            for (int i = (idinicio==0)?1:idinicio; i < idfin; i++) {
                DimensionKinetic pto1 = super.get(i - 1).getDimensionKinetic();
                DimensionKinetic pto2 = super.get(i).getDimensionKinetic();
                
                if ((dkc.compare(pto1, pto2)>0) ||
                        ((dkc.compare(pto1, pto2)==0) &&
                        (dkcwd.compare(pto1, pto2)>0))){
                    out.println("Error de orden en t="+valueOf(time)+", delta="+valueOf(delta)+", posiciones "+valueOf(i-1)+" y "+valueOf(i)+":"+valueOf(pto1.getPosition(time+delta))+" - "+valueOf(pto2.getPosition(time+delta))); 
                    num += 1;
                }

                if (num>0) {
                    break;
                }
            }
            return (num == 0);
        } // else
        return true;
    }

    /**
     * Método para ordenar la lista cinética dado un tiempo y un delta
     *
     * @param idInicial indice inicial, inclusive
     * @param idFinal indice final, inclusive
     * @param time tiempo para ordenar la lista cinética
     */
    public synchronized void insertionSort(int idInicial, int idFinal, double time) {
        Wrapper temp;
        List<Wrapper> arraySentinel = this.subList(idInicial, idFinal);
        DimensionKineticComparator dkc = new DimensionKineticComparator(time);
        
        arraySentinel.add(0, new Wrapper(new DimensionKinetic(0.0, NEGATIVE_INFINITY), -1, false));

        for (int i = 1; i < arraySentinel.size(); i++) {
            temp = arraySentinel.get(i);
            int j = i;
            /**
             * Stabil weil sortieren[j-1] > temp! Wäre nicht stabil wenn >=!
             */
            while (dkc.compare(arraySentinel.get(j - 1).getDimensionKinetic(), temp.getDimensionKinetic())>0) {
                j--;
            }

            // lo muevo
            temp = arraySentinel.remove(i);
            arraySentinel.add(j, temp);
        }

        arraySentinel.remove(0);
    }    
    
    /**
     * Método para ordenar la lista cinética dado un tiempo y un delta
     *
     * @param idInicial indice inicial, inclusive
     * @param idFinal indice final, inclusive
     * @param time tiempo para ordenar la lista cinética
     * @param delta tiempo para resolver las colisiones múltiples
     */
    public synchronized void insertionSort(int idInicial, int idFinal, double time, double delta) {
        Wrapper temp;
        List<Wrapper> arraySentinel = this.subList(idInicial, idFinal);
        DimensionKineticComparatorWithDelta dkcwd = new DimensionKineticComparatorWithDelta(time, delta);
        
        arraySentinel.add(0, new Wrapper(new DimensionKinetic(0.0, NEGATIVE_INFINITY), -1, false));

        for (int i = 1; i < arraySentinel.size(); i++) {
            temp = arraySentinel.get(i);
            int j = i;
            /**
             * Stabil weil sortieren[j-1] > temp! Wäre nicht stabil wenn >=!
             */
            while (dkcwd.compare(arraySentinel.get(j - 1).getDimensionKinetic(), temp.getDimensionKinetic())>0) {
                j--;
            }

            // lo muevo
            temp = arraySentinel.remove(i);
            arraySentinel.add(j, temp);
        }

        arraySentinel.remove(0);
    }    
    
    /**
     *
     * @param time
     */
    public void sort(double time) {
        this.insertionSort(0, this.size(), time);
    }
    
    /**
     *
     * @param time
     * @param delta
     */
    public void sort(double time, double delta){
        this.insertionSort(0, this.size(), time, delta);
    }
    
    
    /**
     *
     */
    @Override
    public void clear() {
        super.clear();
    }

    /**
     *
     * @param pos
     * @return
     */
    @Override
    public Wrapper get(int pos) {
        return super.get(pos);
    }

    /**
     *
     * @return
     */
    @Override
    public int size() {
        return super.size();
    }

    /**
     *
     * @return
     */
    public Intersections obtainIntersections() {
        Intersections interseccs = new Intersections();
        List<Integer> aux = new ArrayList<>();

        for (Wrapper wrapper : this) {
            if (!wrapper.isMaximum()){
                aux.add(wrapper.getObjectId());
            } else {
                int pos = aux.indexOf(wrapper.getObjectId());
                aux.remove(pos);
                
                for (Integer value : aux) {
                    if (wrapper.getObjectId()!=value){
                        interseccs.add(new PairInteger(wrapper.getObjectId(), value));
                    }
                }
            }
        }
        
        return interseccs;
    }

    /**
     * Se usa solo cuando se encuentran multiples eventos dentro de la lista
     * cinetica. Me retorna el rango de indices, dentro de la actual lista
     * cinetica, la que será usada por la cola de eventos para borrar los
     * eventos cuyos indices se encuentran dentro del rango retornado por este
     * metodo, incluidos los extremos.
     *
     * @param time momento donde se evaluará la KSL
     * @param delta
     * @return Par de indices de la lista [inicio, fin] que me indican
     */
    public Pair<Integer> obtainRangeToDelete(double time, double delta) {
        // evaluo la ksl en el tiempo ingresado como parametro y dado que está
        // ordenada se revisa entre que indices se enuentran en la misma posicion
        // en el mismo tiempo.
        int minima = -1;
        int maxima = -1;

        DimensionKineticComparatorWithDelta dkcwd = new DimensionKineticComparatorWithDelta(time, delta);

        if (this.size() >= 3) {
            for (int i = 0; i <= this.size() - 3; i++) {
                DimensionKinetic posxi = this.get(i).getDimensionKinetic();
                DimensionKinetic posximas1 = this.get(i + 1).getDimensionKinetic();
                DimensionKinetic posximas2 = this.get(i + 2).getDimensionKinetic();

                if ((dkcwd.compare(posxi, posximas1) != -1) && (dkcwd.compare(posximas1, posximas2) != -1)) {
                    minima = i;
                    int j = 2;

                    while ((j < this.size())
                            && (dkcwd.compare(this.get(i + j - 1).getDimensionKinetic(), this.get(i + j).getDimensionKinetic()) != -1)) {
                        maxima = i + j;
                        j++;
                    }
                }
            }

            if ((minima == -1) || (maxima == -1)) {
                return null;
            } else {
                return new PairInteger(minima, maxima);
            }
        }
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        String s = "";

        for (Wrapper next : this) {
            s = s.concat(next.toString()).concat(" ");
        }
        
        return s;
    }

    /**
     * Dado un objectId se debe encontrar el par de indices de la KSL
     *
     * @param objectId objectId del objeto
     * @return par de índices dentro de la lista donde se encuentran los
     * elementos del objectId
     */
    /*public PairInteger findIds(int objectId) {
        int minId = -1, maxId = -1;

        for (int i = 0; i < this.size(); i++) {
            Wrapper wrap = this.get(i);

            if (wrap.getObjectId() == objectId) {
                if (!wrap.isMaximum()) {
                    minId = i;
                } else {
                    maxId = i;
                }
            }
        }

        return new PairInteger(minId, maxId);
    }*/

    /**
     *
     * @param first
     * @param second
     * @return
     */
    public List<Pair<Integer>> obtainCompletePairsList(Integer first, Integer second) {
        List<Pair<Integer>> lista = new ArrayList<>();

        for (int i = first; i <= second - 1; i++) {
            for (int j = i + 1; j <= second; j++) {
                int idx0 = this.get(i).getObjectId();
                int idx1 = this.get(j).getObjectId();
                Pair<Integer> parTemp = new PairInteger(idx0, idx1);
                
                if ((idx0 != idx1) && (!lista.contains(parTemp))) {
                    lista.add(parTemp);
                }
            }
        }
        return lista;
    }

    /*private void llenarListaPosiciones() {
        List<Integer> listaTemp = new ArrayList<>(this.size());

        int i = 0;
        for (Wrapper wrap : this) {
            if (wrap.isMaximum()) {
                listaTemp.set(2 * wrap.getObjectId() + 1, i++);
            } else {
                listaTemp.set(2 * wrap.getObjectId(), i++);
            }
        }

        for (int j = 0; j < listaTemp.size() / 2; j++) {
            listaPosiciones.add(new PairInteger(listaTemp.get(j), listaTemp.get(j + 1)));
        }
    }*/

    /**
     *
     * @param rangeToDelete
     * @param time
     */
    public void showPositions(Pair<Integer> rangeToDelete, double time) {
        out.println("t=" + time);
        for (int i = rangeToDelete.getFirst(); i <= rangeToDelete.getSecond(); i++) {
            out.print(super.get(i).toString() + "=" + super.get(i).getDimensionKinetic().getPosition(time) + " ");
        }
        out.println("");
    }

    /**
     *
     * @param time
     */
    public void printPositions(double time) {
        this.forEach((wrapper) -> {
            out.print(wrapper.getDimensionKinetic().getPosition(time) + " ");
        });
        out.println("");
    }
    
    /**
     * Se revisa si alrededor del indice pos existen 3 o más elementos de la
     * lista en la misma posicion, al mismo tiempo.
     * @param time tiempo donde evaluar la lista
     * @param pos0 posición extremo izquierdo donde se revisará alrededor si hay un evento múltiple.
     * @param pos1 posición extremo derecho donde se revisará alrededor si hay un evento múltiple.
     * @return verdadero si existe un evento múltiple.
     */
    public boolean existsMultipleEvents(double time, int pos0, int pos1){
        DimensionKineticComparator dkc = new DimensionKineticComparator(time);
        
        List<Wrapper> subLista;
        if (pos0==0){
            subLista = super.subList(pos0, pos0+3);
        } else if (pos1==super.size()-1){
            subLista = super.subList(pos1-2, super.size());
        } else {
            subLista = super.subList(pos0-1, pos1+2);
        }
        
        boolean resultado = false;
        
        for (int i=2; i<subLista.size(); i++) {
            DimensionKinetic dk0 = subLista.get(i-2).getDimensionKinetic();
            DimensionKinetic dk1 = subLista.get(i-1).getDimensionKinetic();
            DimensionKinetic dk2 = subLista.get(i).getDimensionKinetic();
            
            resultado = resultado || 
                    ((dkc.compare(dk0, dk1)==0) && (dkc.compare(dk1, dk2)==0));
            
            if (resultado) {
                break;
            }
        }
        
        return resultado;
    }
}
