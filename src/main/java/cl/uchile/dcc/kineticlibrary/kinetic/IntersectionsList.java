package cl.uchile.dcc.kineticlibrary.kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import cl.uchile.dcc.staticlibrary.primitives.Intersections;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author hmoraga
 */
public class IntersectionsList extends Observable {
    List<Intersections> lista;

    /**
     *
     */
    public IntersectionsList() {
        this.lista = new LinkedList<>();
    }

    /**
     *
     * @param c
     */
    public IntersectionsList(Collection<? extends Intersections> c) {
        this.lista = new LinkedList<>(c);
    }

    /**
     *
     */
    public void clear() {
        this.lista.clear();
    }

    /**
     *
     * @param e
     * @return
     */
    public boolean add(Intersections e) {
        return this.lista.add(e);
    }

    /**
     *
     * @param c
     * @return
     */
    public List<Intersections> addAllLists(Collection<? extends Intersections> c) {
        this.lista.addAll(c);
        
        return this.lista;
    }

    /**
     *
     * @return
     */
    public Intersections getColisiones() {
        int menor = 0;

        //encontrar lista con menos elementos
        for (int i = 0; i < this.lista.size(); i++) {
            if (this.lista.get(i).size() < this.lista.get(menor).size()) {
                menor = i;
            }
        }

        // separo momentaneamente la lista de menos elementos del resto
        Intersections listaMenor = this.lista.remove(menor);
        Intersections salida = new Intersections(listaMenor);

        // reviso el resto de las listas con la seleccionada
        if (listaMenor.size() > 0) {
            for (Intersections otraLista : lista) {
                salida.retainAll(otraLista);
            }
        }

        // vuelvo this al esto original
        this.lista.add(menor, listaMenor);

        return salida;
    }

    /**
     *
     * @param i
     * @param intersecciones
     */
    public void set(int i, Intersections intersecciones) {
        this.lista.set(i, intersecciones);
    }

    /**
     *
     * @param o
     */
    public void setObserver(Observer o) {
        super.addObserver(o);
    }

    /**
     *
     * @param dimension
     * @return
     */
    public Intersections get(int dimension) {
        return this.lista.get(dimension);
    }

    /**
     *
     * @return
     */
    public int size() {
        return this.lista.size();
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return this.lista.toString();
    }

}
