/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.events;

/**
 *
 * @author hmoraga
 */
abstract class Event implements Comparable<Event> {
    protected double time;
    protected int dimension;

    /**
     *
     * @param time
     * @param dimension
     */
    public Event(double time, int dimension) {
        this.time = time;
        this.dimension = dimension;
    }

    /**
     *
     * @return
     */
    public double getTime() {
        return this.time;
    }

    /**
     *
     * @return
     */
    public int getDimension() {
        return this.dimension;
    }

    /**
     *
     * @param time
     */
    public void setTime(double time) {
        this.time = time;
    }

    /**
     *
     * @param dimension
     */
    public void setDimension(int dimension) {
        this.dimension = dimension;
    }
}
