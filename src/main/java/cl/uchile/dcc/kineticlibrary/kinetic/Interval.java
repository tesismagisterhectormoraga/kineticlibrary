package cl.uchile.dcc.kineticlibrary.kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static java.lang.Math.abs;
import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class Interval {

    /**
     *
     */
    public DimensionKinetic minimum, 

    /**
     *
     */
    maximum;

    /**
     *
     * @param min
     * @param max
     */
    public Interval(DimensionKinetic min, DimensionKinetic max) {
        this.minimum = min;
        this.maximum = max;
    }

    /**
     *
     * @param time
     * @return
     */
    public double getMinimum(double time) {
        return this.minimum.getPosition(time);
    }

    /**
     *
     * @param minimum
     */
    public void setMinimum(DimensionKinetic minimum) {
        this.minimum = minimum;
    }

    /**
     *
     * @param time
     * @return
     */
    public double getMaximum(double time) {
        return this.maximum.getPosition(time);
    }

    /**
     *
     * @param maximum
     */
    public void setMaximum(DimensionKinetic maximum) {
        this.maximum = maximum;
    }

    /**
     *
     * @param time
     * @return
     */
    public double getLength(double time) {
        return (this.maximum.getV() - this.minimum.getV()) * time + (this.maximum.getK() - this.minimum.getK());
    }

    /**
     *
     * @param time
     * @return
     */
    public double getCenter(double time) {
        return this.getMinimum(time) + this.getLength(time) / 2;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "Interval{" + "minimum=" + this.minimum.toString() + ", maximum=" + this.maximum.toString() + '}';
    }

    /**
     *
     * @param other
     * @param time
     * @return
     */
    public boolean intersects(Interval other, double time) {
        //calculo el largo de ambos intervalos
        double miLargo = this.getLength(time);
        double otherLargo = other.getLength(time);
        // calculo los centros de ambos intervalos
        double miCentro = this.getCenter(time);
        double otherCentro = other.getCenter(time);

        return ((2 * abs(miCentro - otherCentro) - (miLargo + otherLargo)) <= 0);
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.minimum);
        hash = 37 * hash + Objects.hashCode(this.maximum);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Interval other = (Interval) obj;
        if (!Objects.equals(this.minimum, other.minimum)) {
            return false;
        }
        return Objects.equals(this.maximum, other.maximum);
    }
}
