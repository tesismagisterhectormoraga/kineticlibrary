package cl.uchile.dcc.kineticlibrary.kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Objects;

/**
 *
 * @author hmoraga
 */
public class Wrapper {
    private DimensionKinetic pos;   // posicion 
    private int objectId;
    private boolean maximum;  // whether the point is the box extrema or not

    /**
     *
     * @param p
     * @param objectId
     * @param isMaximum
     */
    public Wrapper(DimensionKinetic p, int objectId, boolean isMaximum) {
        this.pos = p;
        this.objectId = objectId;
        this.maximum = isMaximum;
    }

    /**
     *
     * @return
     */
    public DimensionKinetic getDimensionKinetic() {
        return this.pos;
    }

    /**
     *
     * @param p
     */
    public void setDimensionKinetic(DimensionKinetic p) {
        this.pos = p;
    }

    /**
     *
     * @return
     */
    public int getObjectId() {
        return this.objectId;
    }

    /**
     *
     * @param objectId
     */
    public void setObjectId(int objectId) {
        this.objectId = objectId;
    }

    /**
     *
     * @return
     */
    public boolean isMaximum() {
        return this.maximum;
    }

    /**
     *
     * @param maximum
     */
    public void setIsMaximum(boolean maximum) {
        this.maximum = maximum;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return ((this.maximum)?"q":"p")+this.objectId;
    }

    /**
     *
     * @param other
     * @return
     */
    public double getEventTime(Wrapper other) {
        return (this.pos.getIntersection(other.getDimensionKinetic()));
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.pos);
        hash = 47 * hash + this.objectId;
        hash = 47 * hash + (this.maximum ? 1 : 0);
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final Wrapper other = (Wrapper) obj;
        if (!Objects.equals(this.pos, other.pos)) {
            return false;
        }
        if (this.objectId != other.objectId) {
            return false;
        }
        return this.maximum == other.maximum;
    }
    
    
}
