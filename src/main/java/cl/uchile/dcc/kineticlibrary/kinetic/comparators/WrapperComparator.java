/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.comparators;

import java.util.Comparator;
import cl.uchile.dcc.kineticlibrary.kinetic.Wrapper;

/**
 *
 * @author hmoraga
 */
public class WrapperComparator implements Comparator<Wrapper> {
    private final double time; //para ordenar

    /**
     *
     * @param time
     */
    public WrapperComparator(double time) {
        this.time = time;
    }

    /**
     *
     * @param i1
     * @param i2
     * @return
     */
    @Override
    public int compare(Wrapper i1, Wrapper i2) {
        if (i1.getDimensionKinetic().getPosition(this.time) - i2.getDimensionKinetic().getPosition(this.time) < 0) {
            return -1;
        } else if (i1.getDimensionKinetic().getPosition(this.time) - i2.getDimensionKinetic().getPosition(this.time) > 0) {
            return 1;
        } else {
            return new WrapperComparatorWithDelta(this.time, 1E-7).compare(i1, i2);
        }
    }
}
