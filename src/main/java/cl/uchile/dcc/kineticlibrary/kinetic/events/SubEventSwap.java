/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.events;

import static java.lang.Double.doubleToLongBits;

/**
 *
 * @author hmoraga
 */
public class SubEventSwap {

    /**
     *
     */
    public double time;

    /**
     *
     */
    public int id0, 

    /**
     *
     */
    id1;

    /**
     *
     * @param time
     * @param id0
     * @param id1
     */
    public SubEventSwap(double time, int id0, int id1) {
        this.time = time;
        this.id0 = id0;
        this.id1 = id1;
    }

    /**
     *
     * @return
     */
    public double getTime() {
        return this.time;
    }

    /**
     *
     * @param time
     */
    public void setTime(double time) {
        this.time = time;
    }

    /**
     *
     * @return
     */
    public int getId0() {
        return this.id0;
    }

    /**
     *
     * @param id0
     */
    public void setId0(int id0) {
        this.id0 = id0;
    }

    /**
     *
     * @return
     */
    public int getId1() {
        return this.id1;
    }

    /**
     *
     * @param id1
     */
    public void setId1(int id1) {
        this.id1 = id1;
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (int) (doubleToLongBits(this.time) ^ (doubleToLongBits(this.time) >>> 32));
        hash = 23 * hash + this.id0;
        hash = 23 * hash + this.id1;
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final SubEventSwap other = (SubEventSwap) obj;
        if (doubleToLongBits(this.time) != doubleToLongBits(other.time)) {
            return false;
        }
        if (this.id0 != other.id0) {
            return false;
        }
        return this.id1 == other.id1;
    }
}
