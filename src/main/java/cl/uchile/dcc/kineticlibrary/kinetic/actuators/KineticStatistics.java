package cl.uchile.dcc.kineticlibrary.kinetic.actuators;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import cl.uchile.dcc.kineticlibrary.kinetic.IntersectionsList;
import cl.uchile.dcc.staticlibrary.primitives.Intersections;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.sap.Statistics;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hmoraga
 */
public class KineticStatistics extends Observable implements Statistics {
    private double areaTotal, areaOcupadaInicial;
    private Pair<Double> intervaloSimulacion;
    // especifico de la simulacion cinetica
    private int eventosSimples, eventosMultiples;
    private final List<String> texto;

    /**
     * Constructor con parametros
     *
     */
    public KineticStatistics() {
        this.texto = new LinkedList<>();
    }

    /**
     *
     * @return
     */
    @Override
    public double getTotalArea() {
        return this.areaTotal;
    }

    /**
     *
     * @param areaTotal
     */
    @Override
    public void setTotalArea(double areaTotal) {
        this.areaTotal = areaTotal;
    }

    /**
     *
     * @return
     */
    @Override
    public double getInitialOcupatedArea() {
        return this.areaOcupadaInicial;
    }

    /**
     *
     * @param areaOcupadaInicial
     */
    @Override
    public void setInitialOcupatedArea(double areaOcupadaInicial) {
        this.areaOcupadaInicial = areaOcupadaInicial;
    }

    /**
     *
     * @return
     */
    public int getSimpleEvents() {
        return this.eventosSimples;
    }

    /**
     *
     * @param eventosSimples
     */
    public void setSimpleEvents(int eventosSimples) {
        this.eventosSimples = eventosSimples;
    }

    /**
     *
     * @return
     */
    public int getMultipleEvents() {
        return this.eventosMultiples;
    }

    /**
     *
     * @param eventosMultiples
     */
    public void setMultipleEvents(int eventosMultiples) {
        this.eventosMultiples = eventosMultiples;
    }

    /**
     * Ingreso el par de objetos que involucran el evento simple
     *
     * @param time
     * @param par
     * @param dimension
     * @param condicion
     */
    public void addSimpleEvent(double time, Pair<Integer> par, int dimension, String condicion) {
        if (condicion.equals("agregar")) {
            this.texto.add(time+", ["+par.toString()+","+((dimension==0)?"X":"Y")+"]+");
        } else {
            this.texto.add(time+", ["+par.toString()+","+((dimension==0)?"X":"Y")+"]-");
        }
        this.eventosSimples++;
    }

    /**
     *
     * @param time
     * @param listaPares
     */
    public void addAllSimpleEvents(double time, List<Pair<Integer>> listaPares) {
        this.texto.add(time + ", " + listaPares.toString());
        this.eventosSimples+=listaPares.size();
    }

    /**
     *
     * @param time
     * @param listaPares
     * @param dimension
     */
    public void addMultipleEvent(double time, List<Pair<Integer>> listaPares, int dimension) {
        this.texto.add(time + ", " + listaPares.toString()+","+((dimension==0)?"X":"Y"));
        this.eventosMultiples++;
    }

    /**
     *
     * @return
     */
    @Override
    public List<String> getText() {
        List<String> s = new ArrayList<>();

        s.add("areaTotal:" + this.areaTotal);
        s.add("tiempoTotalSimulado:" + (this.intervaloSimulacion.getSecond() - this.intervaloSimulacion.getFirst()));
        s.add("areaOcupadaInicial:" + this.areaOcupadaInicial);
        s.add("eventosSimples:" + this.eventosSimples);
        s.add("eventosMultiples:" + this.eventosMultiples);
        this.texto.forEach((line) -> {
            s.add(line);
        });

        return s;
    }

    /**
     *
     * @param o
     */
    public void setObserver(Observer o) {
        super.addObserver(o);
    }

    /**
     *
     * @return
     */
    public boolean hasMultipleEvents() {
        return (this.eventosMultiples != 0);
    }

    /**
     *
     * @param arch
     * @throws FileNotFoundException
     */
    @Override
    public void writeToFile(Path arch) throws FileNotFoundException {
        try (Formatter output = new Formatter(arch.toFile())) {
            
            List<String> textoLocal = this.getText();
            
            textoLocal.forEach((s) -> {
                output.format("%s%n", s);
                output.flush();
            });
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Pair<Double> getSimulationInterval() {
        return this.intervaloSimulacion;
    }

    /**
     *
     * @param intervaloSimulacion
     */
    @Override
    public void setSimulationInterval(Pair<Double> intervaloSimulacion) {
        this.intervaloSimulacion = intervaloSimulacion;
    }

    /**
     *
     * @param actualTime
     * @param intersecciones
     */
    public void addInitialLists(double actualTime, IntersectionsList intersecciones) {
        this.texto.add("" + actualTime);
        
        for (int dim = 0; dim < intersecciones.size(); dim++) {
            Intersections interTemporalPorDimension = intersecciones.get(dim);
            List<Pair<Integer>> listaColisiones = interTemporalPorDimension.getCollisionsList();
            this.texto.add((listaColisiones==null)?null:listaColisiones.toString());
        }
    }
}
