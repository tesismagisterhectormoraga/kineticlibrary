/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.events;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.System.out;
import java.util.Iterator;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.PriorityQueue;
import static cl.uchile.dcc.kineticlibrary.kinetic.events.EnumerateEvents.CALCULAREVENTOSACTUALES;
import static cl.uchile.dcc.kineticlibrary.kinetic.events.EnumerateEvents.CALCULAREVENTOSINICIALES;
import static cl.uchile.dcc.kineticlibrary.kinetic.events.EnumerateEvents.EVENTOSIMPLE;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;

/**
 *
 * @author hmoraga
 */
public class EventsQueue extends Observable {
    private PriorityQueue<Event> colaEventos;
    private double initialTime, finalTime, currentTime;
    private boolean verbose;
    //private double minimalTimeStep;

    /**
     *
     * @param tiempos un par [t0, tfin] de la simulacion
     * @param verbose si true imprime mensajes por pantalla
     */
    public EventsQueue(Pair<Double> tiempos, boolean verbose) {
        this(tiempos, 11, verbose);
    }

    /**
     * 
     * @param tiempos un par [t0, tfin] de la simulacion
     * @param cantidadInicial tamaño ninicial de la cola de eventos
     * @param verbose si true imprime mensajes por pantalla
     */
    public EventsQueue(Pair<Double> tiempos, int cantidadInicial, boolean verbose) {
        this.initialTime = tiempos.getFirst();
        this.currentTime = tiempos.getFirst();
        this.finalTime = tiempos.getSecond();
        this.colaEventos = new PriorityQueue<>(cantidadInicial);
        this.verbose = verbose;
    }

    /**
     *
     */
    public void calculateInitialEvents() {
        super.setChanged();
        super.notifyObservers(CALCULAREVENTOSINICIALES);
    }

    /**
     * método para calcular eventos desde el tiempo actual hasta el tfinal. Se
     * usa para cuando invalido la listaKSL y la de colisiones.
     *
     * @param includeCurrentTime true si se debe incluir el tiempo actual o no
     */
    public void calculateCurrentEvents(boolean includeCurrentTime) {
        super.setChanged();
        super.notifyObservers(CALCULAREVENTOSACTUALES);
    }

    /**
     *
     * @return
     */
    public double getInitialTime() {
        return this.initialTime;
    }

    /**
     *
     * @return
     */
    public double getFinalTime() {
        return this.finalTime;
    }

    /**
     *
     * @return
     */
    public double getCurrentTime() {
        return this.currentTime;
    }

    /**
     *
     * @param tpo_actual
     */
    public void setCurrentTime(double tpo_actual) {
        this.currentTime = tpo_actual;
    }

    /**
     *
     * @param e
     * @return
     */
    public boolean add(Event e) {
        return this.colaEventos.add(e);
    }

    /**
     *
     * @return
     */
    public Event peek() {
        return this.colaEventos.peek();
    }

    /**
     *
     * @return
     */
    public Event poll() {
        return this.colaEventos.poll();
    }

    /**
     * metodo para procesar los eventos de la cola
     */
    public void processEvents() {
        int contador = 0;
        // se supone que existe una lista de colisiones inicial, 
        while (!this.colaEventos.isEmpty()) {
            if (this.verbose) {
                out.println("Evento " + (++contador));
            }

            // asumo que todos los eventos son simples (a menos que el actuador diga
            // lo contrario)
            super.setChanged();
            super.notifyObservers(EVENTOSIMPLE);
        }
    }

    /**
     *
     * @param o
     */
    public void setObserver(Observer o) {
        super.addObserver(o);
    }

    /**
     *
     * @param masEventos
     * @param dimension
     */
    public void addAll(List<SubEventSwap> masEventos, int dimension) {
        masEventos.stream()
                .filter((subEv) -> (subEv.getTime() > this.currentTime && subEv.getTime() < this.finalTime))
                .forEach((SubEventSwap subEv) -> {
                    this.colaEventos.add(new SwapEvent(subEv.getId0(), subEv.getId1(), subEv.getTime(), dimension));
                });
    }

    /**
     *
     * @return
     */
    public boolean isEmpty() {
        return this.colaEventos.isEmpty();
    }

    /**
     *
     * @param time
     * @param idMin
     * @param idMax
     * @param dimension
     */
    public synchronized void deleteMultipleEvents(double time, int idMin, int idMax, int dimension) {
        Iterator<Event> it = this.colaEventos.iterator();

        while (it.hasNext()) {
            SwapEvent ev = (SwapEvent) it.next();

            if ((ev.getTime() == time) && (ev.getDimension() == dimension)) {
                if ((idMin <= ev.getId0() && ev.getId0() <= idMax)
                        || (idMin <= ev.getId1() && ev.getId1() <= idMax)) {
                    it.remove();
                }
            } else if (ev.getTime() > time) {
                break;
            }
        }
    }

    /**
     *
     */
    public void clear() {
        this.colaEventos.clear();
    }
    
    /**
     * Solo existe un evento múltiple si dos o más eventos tienen en común un id de la KSL,
     * para la misma dimensión en el mismo instante de tiempo. Para probar esto, se debe extraer
     * al menos 2 de los eventos y probar si existen los casos descritos más arriba.
     * @return true si hay múltiples eventos, false en caso contrario.
     */
    public boolean existMultipleEvents(){
        boolean response = false;
        
        if (this.colaEventos.size()>=2){
            SwapEvent ev0 = (SwapEvent)this.colaEventos.poll(); // saco el primer evento de la cola
            SwapEvent ev1 = (SwapEvent)this.colaEventos.peek(); // consulto el evento siguiente (sin sacarlo)
            
            // debe existir al menos 1 ID repetido entre dos eventos consecutivos
            // en el mismo tiempo y la misma dimension
            if ((ev0.getTime()==ev1.getTime()) && (ev0.getDimension()==ev1.getDimension())){
                if ((ev0.getId0()==min(ev1.getId0(), ev1.getId1())) || 
                        (ev0.getId0()==max(ev1.getId0(), ev1.getId1())) ||
                        (ev0.getId1()==min(ev1.getId0(), ev1.getId1())) || 
                        (ev0.getId1()==max(ev1.getId0(), ev1.getId1()))) {
                    response = true;
                }
            }
            
            // vuelvo a colocar los eventos en la cola para dejarla como estaba
            this.colaEventos.add(ev0);
        }
        return response;
    }

    /**
     *
     * @param actualTime
     * @param dimension
     * @return
     */
    public Pair<Integer> obtainRangeToDelete(double actualTime, int dimension) {
        Pair<Integer> lastIds = new PairInteger(-1, -1);
        int minimo=-1, maximo=-1;
        while ((this.peek().getTime() == actualTime) && 
                (this.peek().getDimension() == dimension) &&
                !this.isEmpty()){
            // saco el evento
            SwapEvent ev = (SwapEvent)this.poll();
            
            if ((minimo == -1) && (maximo == -1)){
                minimo = min(ev.getId0(), ev.getId1());
                maximo = max(ev.getId0(), ev.getId1());
                lastIds = new PairInteger(minimo, maximo);
            } else {
                if ((ev.getId0()==lastIds.getFirst()) || (ev.getId0()==lastIds.getSecond()) ||
                        (ev.getId1()==lastIds.getFirst()) || (ev.getId1()==lastIds.getSecond())){
                    minimo = min(minimo, min(ev.getId0(), ev.getId1()));
                    maximo = max(maximo, max(ev.getId0(), ev.getId1()));
                    lastIds = new PairInteger(ev.getId0(), ev.getId1());
                }
            }
        }

        return new PairInteger(minimo, maximo);
    }
}
