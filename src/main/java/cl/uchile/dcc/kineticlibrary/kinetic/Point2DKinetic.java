package cl.uchile.dcc.kineticlibrary.kinetic;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hmoraga
 */
public class Point2DKinetic {
    private DimensionKinetic[] kinetics;

    /**
     *
     * @param vx
     * @param kx
     * @param vy
     * @param ky
     */
    public Point2DKinetic(double vx, double kx, double vy, double ky) {
        this.kinetics = new DimensionKinetic[]{new DimensionKinetic(vx, kx), new DimensionKinetic(vy, ky)};
    }

    /**
     *
     * @param dimX
     * @param dimY
     */
    public Point2DKinetic(DimensionKinetic dimX, DimensionKinetic dimY) {
        this.kinetics = new DimensionKinetic[]{dimX, dimY};
    }

    /**
     *
     * @param time
     * @return
     */
    public Point2D getPosition(double time) {
        return new Point2D(this.kinetics[0].getPosition(time), this.kinetics[1].getPosition(time));
    }

    /**
     *
     * @param time
     */
    public void setPosition(double time) {
        for (DimensionKinetic kin : this.kinetics) {
            kin.setK(kin.getPosition(time));
        }
    }

    /**
     *
     * @param velocidades
     */
    public void setVelocidad(Pair<Double> velocidades) {
        this.kinetics[0].setV(velocidades.getFirst());
        this.kinetics[1].setV(velocidades.getSecond());
    }

    /**
     *
     * @return
     */
    public Pair<Double> getVelocidad() {
        return new PairDouble(this.kinetics[0].getV(), this.kinetics[1].getV());
    }
}
