package cl.uchile.dcc.kineticlibrary.kinetic.events;

import cl.uchile.dcc.kineticlibrary.kinetic.IntersectionsList;
import java.util.Observable;
import java.util.Observer;
import static java.util.logging.Level.SEVERE;
import static java.util.logging.Logger.getLogger;
import cl.uchile.dcc.kineticlibrary.kinetic.KSLList;
import cl.uchile.dcc.kineticlibrary.kinetic.actuators.*;

/**
 *
 * @author hmoraga
 */
public class EventsObserver implements Observer {
    private final KSLList listaKSL;
    private final IntersectionsList colisiones;
    private final EventsQueue colaEventos;
    private final KineticStatistics estadisticas;
    private final boolean verbose;

    /**
     *
     * @param listaKSL
     * @param colisionesPorDimension
     * @param colaEventos
     * @param estadisticas
     * @param verbose
     */
    public EventsObserver(KSLList listaKSL, IntersectionsList colisionesPorDimension, EventsQueue colaEventos,
            KineticStatistics estadisticas, boolean verbose) {
        this.listaKSL = listaKSL;
        this.colisiones = colisionesPorDimension;
        this.colaEventos = colaEventos;
        this.estadisticas = estadisticas;
        this.verbose = verbose;
    }

    /**
     *
     * @param o objeto observable
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {
        if (arg.equals(EnumerateEvents.CALCULAREVENTOSINICIALES)) {
            try {
                Actuator.structuresInitialization(this.colaEventos, this.listaKSL, this.colisiones, this.estadisticas, this.verbose);
            } catch (Exception ex) {
                getLogger(EventsObserver.class.getName()).log(SEVERE, "Error al inicializar estructuras:{0}", ex);
            }
        } else if (arg.equals(EnumerateEvents.EVENTOSIMPLE)) {
            // reviso si la listaKSL tiene eventos simples o son multiples
            // para eso pregunto por el evento de la cola y reviso si la
            // listaKSL tiene un evento multiple
            SwapEvent ev = (SwapEvent)this.colaEventos.peek();
            
            if (!listaKSL.get(ev.getDimension()).existsMultipleEvents(ev.getTime(), ev.getId0(), ev.getId1())){
                try {
                    // resuelvo evento simple
                    Actuator.solveSimpleEvent(this.colaEventos, this.listaKSL, this.colisiones, this.estadisticas, this.verbose);
                } catch (Exception ex) {
                    getLogger(EventsObserver.class.getName()).log(SEVERE, "Error en evento simple:{0}", ex);
                }
            } else {
                try {
                    // resuelvo evento multiple
                    Actuator.solveMultipleEvent(this.colaEventos, this.listaKSL, this.colisiones, this.estadisticas, this.verbose);
                } catch (Exception ex) {
                    getLogger(EventsObserver.class.getName()).log(SEVERE, "error en evento multiple:{0}", ex);
                }
            }
        }
    }
}
