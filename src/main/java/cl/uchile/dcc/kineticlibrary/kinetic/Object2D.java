package cl.uchile.dcc.kineticlibrary.kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import cl.uchile.dcc.staticlibrary.poligons.AnyPolygon2D;
import cl.uchile.dcc.staticlibrary.poligons.Polygon2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author hmoraga
 */
public class Object2D {
    private final int id;
    private List<Point2D> listaPuntos;
    private Pair<Double> velocidad;
    private double actualTime;
    private int idMinX, idMinY, idMaxX, idMaxY;

    /**
     *
     * @param id
     * @param listaPuntos
     * @param velocidad
     * @param time
     */
    public Object2D(int id, List<Point2D> listaPuntos,
            Pair<Double> velocidad, double time) {
        if (time < 0) {
            throw new IllegalArgumentException("Tiempo debe ser mayor o igual a 0!");
        }
        this.idMinX = 0;
        this.idMinY = 0;
        this.idMaxX = 0;
        this.idMaxY = 0;

        this.id = id;
        this.actualTime = time;

        this.listaPuntos = new ArrayList<>(listaPuntos);
        this.velocidad = velocidad;

        for (int i = 0; i < listaPuntos.size(); i++) {
            Point2D punto = listaPuntos.get(i);

            if (punto.getX() < listaPuntos.get(this.idMinX).getX()) {
                this.idMinX = i;
            }

            if (punto.getX() > listaPuntos.get(this.idMaxX).getX()) {
                this.idMaxX = i;
            }

            if (punto.getY() < listaPuntos.get(this.idMinY).getY()) {
                this.idMinY = i;
            }

            if (punto.getY() > listaPuntos.get(this.idMaxY).getY()) {
                this.idMaxY = i;
            }
        }
    }

    /**
     *
     * @param id
     * @param listaPuntos
     * @param velocidad
     */
    public Object2D(int id, List<Point2D> listaPuntos,
            Pair<Double> velocidad) {
        this(id, listaPuntos, velocidad, 0.0);
    }

    /**
     * me devuelve la lista de puntos en la posicion actual
     *
     * @return
     */
    public List<Point2D> getListaPuntos() {
        return this.listaPuntos;
    }

    /**
     *
     * @return
     */
    public Pair<Double> getVelocidad() {
        return this.velocidad;
    }

    /**
     *
     * @param velocidad
     */
    public void setVelocidad(Pair<Double> velocidad) {
        this.velocidad = velocidad;
    }

    /**
     *
     * @return
     */
    public int getObjId() {
        return this.id;
    }

    /**
     *
     * @param dim
     * @return
     */
    public List<Wrapper> getWrappers(int dim) {
        List<Wrapper> temp = new ArrayList<>();

        Wrapper p, q;

        if (dim == 0) {
            p = new Wrapper(new DimensionKinetic(this.velocidad.getFirst(), this.listaPuntos.get(this.idMinX).getX()), this.id, false);
            q = new Wrapper(new DimensionKinetic(this.velocidad.getFirst(), this.listaPuntos.get(this.idMaxX).getX()), this.id, true);
        } else {
            p = new Wrapper(new DimensionKinetic(this.velocidad.getSecond(), this.listaPuntos.get(this.idMinY).getY()), this.id, false);
            q = new Wrapper(new DimensionKinetic(this.velocidad.getSecond(), this.listaPuntos.get(this.idMaxY).getY()), this.id, true);
        }

        temp.add(p);
        temp.add(q);

        return temp;
    }

    /**
     *
     * @param time
     * @return
     */
    public List<Point2D> getPosition(double time) {
        List<Point2D> resultado = new ArrayList<>(this.listaPuntos.size());

        this.listaPuntos.stream().map((pto) -> new Point2D(pto.getX()+this.velocidad.getFirst() * (time - this.actualTime), pto.getY()+this.velocidad.getSecond() * (time - this.actualTime))).forEachOrdered((p) -> {
            resultado.add(p);
        });

        return resultado;
    }

    /**
     *
     * @param time
     */
    public void setActualTime(double time) {
        this.actualTime = time;
    }

    /**
     *
     */
    public void close() {
        this.listaPuntos.clear();
        this.velocidad = null;
    }

    /**
     *
     * @return
     */
    public double getArea() {
        Polygon2D poly = new AnyPolygon2D(this.listaPuntos, false);

        return poly.getArea();
    }
}
