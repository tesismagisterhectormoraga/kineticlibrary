/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.events;

/**
 *
 * @author hmoraga
 */
public enum EnumerateEvents {

    /**
     *
     */
    CALCULAREVENTOSINICIALES, 

    /**
     *
     */
    CALCULAREVENTOSACTUALES, 

    /**
     *
     */
    EVENTOSIMPLE, 

    /**
     *
     */
    EVENTOMULTIPLE;
}
