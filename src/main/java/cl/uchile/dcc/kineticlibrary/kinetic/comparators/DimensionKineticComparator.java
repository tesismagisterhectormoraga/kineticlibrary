/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.comparators;

import java.util.Comparator;
import cl.uchile.dcc.kineticlibrary.kinetic.DimensionKinetic;

/**
 *
 * @author hmoraga
 */
public class DimensionKineticComparator implements Comparator<DimensionKinetic> {
    private final double time;

    /**
     *
     * @param time
     */
    public DimensionKineticComparator(double time) {
        this.time = time;
    }

    /**
     *
     * @param i1
     * @param i2
     * @return
     */
    @Override
    public int compare(DimensionKinetic i1, DimensionKinetic i2) {
        //double pos1 = i1.getV()*time + i1.getK();
        //double pos2 = i2.getV()*time + i2.getK();
        double comparacion = (i1.getV()-i2.getV())*this.time + (i1.getK()-i2.getK());
        
        if (comparacion>0) {
            return 1;
        } else if (comparacion<0) {
            return -1;
        } else {
            return 0;
        }
    }
}
