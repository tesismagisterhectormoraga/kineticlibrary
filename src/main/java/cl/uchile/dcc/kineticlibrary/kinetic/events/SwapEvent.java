/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.events;

import static java.lang.Math.min;

/**
 *
 * @author hmoraga
 */
public class SwapEvent extends Event {
    private final int id0, id1; // ids corresponden a LAS POSICIONES en la KSL

    /**
     *
     * @param id0
     * @param id1
     * @param time
     * @param dimension
     */
    public SwapEvent(int id0, int id1, double time, int dimension) {
        super(time, dimension);
        this.id0 = id0;
        this.id1 = id1;
    }

    /**
     *
     * @return
     */
    public int getId0() {
        return this.id0;
    }

    /**
     *
     * @return
     */
    public int getId1() {
        return this.id1;
    }

    /**
     *
     * @param o
     * @return
     * @throws NullPointerException
     */
    @Override
    public int compareTo(Event o) throws NullPointerException {
        SwapEvent ev = (SwapEvent) o;

        if (ev == null) {
            throw new NullPointerException("No es evento Swap");
        }

        if ((this.time < o.time)
                || ((this.time == o.time) && (this.dimension < o.dimension))
                || ((this.time == o.time) && (this.dimension == o.dimension)
                && (min(this.id0, this.id1) < min(ev.getId0(), ev.getId1())))) {
            return -1;
        } else if ((this.time > o.time)
                || ((this.time == o.time) && (this.dimension > o.dimension))
                || ((this.time == o.time) && (this.dimension == o.dimension)
                && (min(this.id0, this.id1) > min(ev.getId0(), ev.getId1())))) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + this.id0;
        hash = 43 * hash + this.id1;
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }

        final SwapEvent other = (SwapEvent) obj;

        return (this.time == other.time) && (this.dimension == other.dimension)
                && (((this.id0 == other.id0) && (this.id1 == other.id1))
                || ((this.id0 == other.id1) && (this.id1 == other.id0)));
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "EV(t=" + this.time + ")" + this.id0 + "_" + this.id1 + ((this.dimension == 0) ? "X" : "Y");
    }
}
