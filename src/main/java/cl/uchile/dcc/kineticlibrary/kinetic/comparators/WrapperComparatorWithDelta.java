/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.comparators;

import java.util.Comparator;
import cl.uchile.dcc.kineticlibrary.kinetic.Wrapper;

/**
 *
 * @author hmoraga
 */
public class WrapperComparatorWithDelta implements Comparator<Wrapper> {
    private final double time, delta; //delta se usa en caso de multiples eventos para desempate

    /**
     *
     * @param time
     * @param delta
     */
    public WrapperComparatorWithDelta(double time, double delta) {
        this.time = time;
        this.delta = delta;
    }

    /**
     *
     * @param i1
     * @param i2
     * @return
     */
    @Override
    public int compare(Wrapper i1, Wrapper i2) {
        return new DimensionKineticComparatorWithDelta(this.time, this.delta).compare(i1.getDimensionKinetic(), i2.getDimensionKinetic());
        /*if (i1.getDimensionKinetic().getPosition(time+delta) < i2.getDimensionKinetic().getPosition(time+delta)){
            return -1;
        } else if (i1.getDimensionKinetic().getPosition(time+delta) > i2.getDimensionKinetic().getPosition(time+delta)){
            return 1;
        } else {
            return 0;
        }*/
    }
}
