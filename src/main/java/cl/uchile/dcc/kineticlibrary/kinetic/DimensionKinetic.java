package cl.uchile.dcc.kineticlibrary.kinetic;

import static java.lang.Double.POSITIVE_INFINITY;
import static java.lang.Double.doubleToLongBits;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author hmoraga
 */
public class DimensionKinetic {
    private double v, k;    // x(t) = v*t+k, v = velocidad, k = pos inicial en t0

    /**
     *
     * @param v
     * @param k
     */
    public DimensionKinetic(double v, double k) {
        this.v = v;
        this.k = k;
    }

    /**
     *
     * @param t
     * @return
     */
    public double getPosition(double t) {
        return this.v * t + this.k;
    }

    /**
     *
     * @return
     */
    public double getV() {
        return this.v;
    }

    /**
     *
     * @param v
     */
    public void setV(double v) {
        this.v = v;
    }

    /**
     *
     * @return
     */
    public double getK() {
        return this.k;
    }

    /**
     *
     * @param k
     */
    public void setK(double k) {
        this.k = k;
    }

    /**
     * Me devuelve el tiempo de interseccion entre dos DimensionKinetic.
     * @param other
     * @return 
     */
    public double getIntersection(DimensionKinetic other) {
        return (this.v == other.getV()) ? POSITIVE_INFINITY
                : (this.k - other.getK()) / (other.getV() - this.v);
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "(v=" + this.v + ", k=" + this.k + ')';
    }

    /**
     *
     * @param border
     * @return
     */
    /*public double getBorderTime(double border) {
        if (this.v != 0) {
            return (border - this.k) / this.v;
        } else {
            return Double.POSITIVE_INFINITY;
        }
    }*/

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + (int) (doubleToLongBits(this.v) ^ (doubleToLongBits(this.v) >>> 32));
        hash = 83 * hash + (int) (doubleToLongBits(this.k) ^ (doubleToLongBits(this.k) >>> 32));
        return hash;
    }

    /**
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final DimensionKinetic other = (DimensionKinetic) obj;
        if (doubleToLongBits(this.v) != doubleToLongBits(other.v)) {
            return false;
        }
        return doubleToLongBits(this.k) == doubleToLongBits(other.k);
    }
}
