package cl.uchile.dcc.kineticlibrary.kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.Math.abs;
import java.util.List;

/**
 *
 * @author hmoraga
 */
public class AABB2DKinetic {
    private DimensionKinetic[] p, q;

    /**
     *
     * @param listaPuntos
     * @param velocidades
     */
    public AABB2DKinetic(List<Point2D> listaPuntos, PairDouble velocidades) {
        assert (listaPuntos.size() > 0);
        double minX = listaPuntos.get(0).getX(), minY = listaPuntos.get(0).getY();
        double maxX = listaPuntos.get(0).getX(), maxY = listaPuntos.get(0).getY();

        // dada la lista de puntos estaticos genero el AABB2DKinetico del objeto
        for (Point2D punto : listaPuntos) {
            if (punto.getX() < minX) {
                minX = punto.getX();
            }

            if (punto.getX() > maxX) {
                maxX = punto.getX();
            }

            if (punto.getY() < minY) {
                minY = punto.getY();
            }

            if (punto.getY() > maxY) {
                maxY = punto.getY();
            }
        }

        // busco el punto inferior izq y el superior derecho de la caja
        this.p = new DimensionKinetic[2];
        this.p[0] = new DimensionKinetic(velocidades.getFirst(), minX);
        this.p[1] = new DimensionKinetic(velocidades.getSecond(), minY);

        this.q = new DimensionKinetic[2];
        this.q[0] = new DimensionKinetic(velocidades.getFirst(), maxX);
        this.q[1] = new DimensionKinetic(velocidades.getSecond(), maxY);

        assert (this.p[0].getK() < this.q[0].getK());
        assert (this.p[1].getK() < this.q[1].getK());
    }

    /**
     *
     * @return
     */
    public double getVx() {
        return this.p[0].getV();
    }

    /**
     *
     * @param vx
     */
    public void setVx(double vx) {
        this.p[0] = new DimensionKinetic(vx, this.p[0].getK());
        this.q[0] = new DimensionKinetic(vx, this.q[0].getK());
    }

    /**
     *
     * @return
     */
    public double getVy() {
        return this.p[1].getV();
    }

    /**
     *
     * @param vy
     */
    public void setVy(double vy) {
        this.p[1] = new DimensionKinetic(vy, this.p[1].getK());
        this.q[1] = new DimensionKinetic(vy, this.q[1].getK());
    }

    /**
     *
     * @param time
     * @return
     */
    public AABB2D getBox(double time) {
        // consulto el valor de p y q en e tiempo time y de ahi calculo la caja
        Point2D pt, qt;

        pt = new Point2D(this.p[0].getPosition(time), this.p[1].getPosition(time));
        qt = new Point2D(this.q[0].getPosition(time), this.q[1].getPosition(time));

        return new AABB2D(new Point2D((pt.getX() + qt.getX()) / 2, (pt.getY() + qt.getY()) / 2),
                abs(pt.getX() - qt.getX()), abs(pt.getY() - qt.getY()));
    }

    /**
     *
     * @param dimension
     * @return
     */
    public Interval getInterval(int dimension) {
        return (dimension == 0) ? new Interval(this.p[0], this.q[0])
                : new Interval(this.p[1], this.q[1]);
    }

    /**
     *
     * @return
     */
    public DimensionKinetic[] getP() {
        return this.p;
    }

    /**
     *
     * @return
     */
    public DimensionKinetic[] getQ() {
        return this.q;
    }
}
