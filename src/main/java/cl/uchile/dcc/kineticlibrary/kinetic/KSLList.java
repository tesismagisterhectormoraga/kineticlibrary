package cl.uchile.dcc.kineticlibrary.kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import cl.uchile.dcc.kineticlibrary.kinetic.comparators.DimensionKineticComparator;
import cl.uchile.dcc.kineticlibrary.kinetic.comparators.DimensionKineticComparatorWithDelta;
import cl.uchile.dcc.staticlibrary.primitives.Intersections;

/**
 *
 * @author hmoraga
 */
public class KSLList extends Observable {
    private List<KineticSortedList> listaKSL;

    /**
     *
     * @param dimensions
     */
    public KSLList(int dimensions) {
        this.listaKSL = new ArrayList<>(dimensions);
    }

    /**
     *
     * @param c
     */
    public KSLList(Collection<? extends KineticSortedList> c) {
        this.listaKSL = new ArrayList<>(c);
    }

    /**
     *
     * @param lista
     */
    public KSLList(KSLList lista) {
        this.listaKSL = lista.listaKSL;
    }

    /**
     *
     */
    public void clear() {
        this.listaKSL.clear();
    }

    /**
     *
     * @param e
     * @return
     */
    public boolean add(KineticSortedList e) {
        return this.listaKSL.add(e);
    }

    /**
     *
     * @return
     */
    public Intersections getIntersections() {
        return new IntersectionsList(getIntersectionsByDimension()).getColisiones();
    }

    /**
     *
     * @return
     */
    public List<Intersections> getIntersectionsByDimension() {
        List<Intersections> listaTemp = new ArrayList<>();

        for (KineticSortedList ksl : listaKSL) {
            listaTemp.add(ksl.obtainIntersections());
        }
        
        return listaTemp;
    }

    /**
     *
     * @return
     */
    public boolean isEmpty() {
        return this.listaKSL.isEmpty();
    }

    /**
     *
     * @return
     */
    public int size() {
        return this.listaKSL.size();
    }

    /**
     *
     * @param i
     * @return
     */
    public KineticSortedList get(int i) {
        return this.listaKSL.get(i);
    }

    /**
     *
     * @param i
     * @param KSL
     */
    public void set(int i, KineticSortedList KSL) {
        this.listaKSL.set(i, KSL);
    }

    /**
     *
     * @return
     */
    public List<KineticSortedList> getKSLList() {
        return this.listaKSL;
    }

    /**
     *
     * @param o
     */
    public void setObserver(Observer o) {
        super.addObserver(o);
    }

    /**
     *
     * @param idx0
     * @param idx1
     * @param dimension
     */
    public void swap(int idx0, int idx1, int dimension) {
        KineticSortedList ksl;
        synchronized (ksl = this.listaKSL.get(dimension)) {
            Wrapper aux = ksl.get(idx0);
            ksl.set(idx0, ksl.get(idx1));
            ksl.set(idx1, aux);
        }
    }

    /**
     *
     * @param time
     * @param deltaTime
     */
    public void sort(double time, double deltaTime) {
        synchronized(this.getKSLList()){
            this.getKSLList().stream().forEach((ksl) -> {
                ksl.sort(time, deltaTime);
            });            
        }
    }

    /**
     *
     * @param time
     */
    public void sort(double time) {
        synchronized(this.getKSLList()){
            this.getKSLList().stream().forEach((ksl) -> {
                ksl.sort(time);
            });
        }
    }

    /**
     *
     * @param time
     * @return
     */
    public boolean listsValidation(double time) {
        boolean resultado = true;
        for (KineticSortedList ksl : this.listaKSL) {
            resultado = resultado && ksl.certificateValidation(time);
        }
        
        return resultado;
    }
    
    /**
     *
     * @param time
     */
    public void imprimir(double time){
        this.listaKSL.forEach((ksl) -> {
            ksl.printPositions(time);
        });
    }

    /**
     *
     * @param dimension
     * @param id0
     * @param id1
     * @param actualTime
     */
    public void showZone(int dimension, int id0, int id1, double actualTime) {
        KineticSortedList local = this.listaKSL.get(dimension);
        
        int inicio = (id0==0)?0:id0-1;
        int fin = (id1==local.size()-1)?local.size()-1:id1+1;
        
        for (int i = inicio; i <= fin; i++) {
            Wrapper wr = local.get(i);
            
            if (wr.isMaximum()){
                out.print("q_"+wr.getObjectId()+"="+wr.getDimensionKinetic().getPosition(actualTime));
            } else {
                out.print("p_"+wr.getObjectId()+"="+wr.getDimensionKinetic().getPosition(actualTime));
            }
            
            if (i==fin) {
                out.println("");
            } else {
                out.print(" ");
            }
        }
    }

    /**
     * 
     * @param dimension
     * @param id0
     * @param id1
     * @param currentTime
     * @param delta
     * @return 
     */
    public boolean validateZone(int dimension, int id0, int id1, double currentTime, double delta) {
        KineticSortedList local = this.get(dimension);
        int inicio = (id0==0)?id0:id0-1;
        int fin = (id1==local.size()-1)?local.size()-1:id1+1;
                
        boolean resultado = true;
        
        DimensionKineticComparator dkc = new DimensionKineticComparator(currentTime);
        DimensionKineticComparatorWithDelta dkcwd = new DimensionKineticComparatorWithDelta(currentTime, delta);
        
        for (int i = inicio+1; i <= fin; i++) {
            DimensionKinetic pos00 = local.get(i-1).getDimensionKinetic();//.getPosition(actualTime);
            DimensionKinetic pos10 = local.get(i).getDimensionKinetic();//.getPosition(actualTime);
            DimensionKinetic pos01 = local.get(i-1).getDimensionKinetic();//.getPosition(actualTime+delta);
            DimensionKinetic pos11 = local.get(i).getDimensionKinetic(); //.getPosition(actualTime+delta);           
            
            resultado = resultado && (dkc.compare(pos00, pos10)==-1 ||
                    (dkc.compare(pos00, pos10)==0 && dkcwd.compare(pos01, pos11)==-1));
            
            if (!resultado) {
                break;
            }
        }
        
        return resultado;
    }
}
