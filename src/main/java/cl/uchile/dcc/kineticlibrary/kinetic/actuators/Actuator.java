/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.actuators;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.System.out;
import java.util.LinkedList;
import java.util.List;
import cl.uchile.dcc.kineticlibrary.kinetic.KSLList;
import cl.uchile.dcc.kineticlibrary.kinetic.KineticSortedList;
import cl.uchile.dcc.kineticlibrary.kinetic.Wrapper;
import cl.uchile.dcc.kineticlibrary.kinetic.events.EventsQueue;
import cl.uchile.dcc.kineticlibrary.kinetic.events.SubEventSwap;
import cl.uchile.dcc.kineticlibrary.kinetic.events.SwapEvent;
import cl.uchile.dcc.kineticlibrary.kinetic.IntersectionsList;
import cl.uchile.dcc.staticlibrary.primitives.Intersections;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;

/**
 *
 * @author hmoraga
 */
public class Actuator {
    private static double currentTime;

    /**
     * @param colaEventos
     * @param listaKSL
     * @param colisiones
     * @param estadisticas
     * @param verbose
     * @throws java.lang.Exception
     */
    public static void structuresInitialization(EventsQueue colaEventos, KSLList listaKSL, IntersectionsList colisiones,
            KineticStatistics estadisticas, boolean verbose) throws Exception {
        currentTime = colaEventos.getInitialTime();

        // ordeno los KSL para el valor inicial de la simulacion
        // uso el sort con delta
        listaKSL.sort(currentTime, 1E-7);

        // valido la lista ordenada
        if (!listaKSL.listsValidation(currentTime)) {
            out.println("LISTAS KSL NO SE INICIAN ORDENADAS!!!!");
        }

        for (int dim = 0; dim < listaKSL.size(); dim++) {
            List<SubEventSwap> listaEventosDimSwap = listaKSL.get(dim).calculateSwapEvents(currentTime, true);

            //agrego lista de eventos a la cola de eventos
            for (SubEventSwap subEv : listaEventosDimSwap) {
                double time = subEv.getTime();

                // debo revisar si los eventos estan entre ]t_inicial;t_final]
                if ((time > colaEventos.getInitialTime()) && (time < colaEventos.getFinalTime())) {
                    colaEventos.add(new SwapEvent(subEv.getId0(), subEv.getId1(), time, dim));
                }
            }
        }

        List<Intersections> addAllLists = colisiones.addAllLists(listaKSL.getIntersectionsByDimension());

        if (verbose) {
            out.println("t=" + currentTime);
            for (int i = 0; i < colisiones.size(); i++) {
                out.println(colisiones.get(i).toString());
            }
            // reviso si la lista cinetica está ordenada inicialmente
            assert(listaKSL.listsValidation(currentTime));
        }        

        // agrego a las estadisticas las listas de intersecciones por dimension
        estadisticas.addInitialLists(currentTime, colisiones);
    }

    /**
     * por defecto todos los eventos son considerados simples y se evalua si el
     * certificado es multiple.
     *
     * @param colaEventos
     * @param listaKSL
     * @param colisiones
     * @param estadisticas
     * @param verbose
     * @throws java.lang.Exception
     */
    public static synchronized void solveSimpleEvent(EventsQueue colaEventos, KSLList listaKSL, IntersectionsList colisiones,
            KineticStatistics estadisticas, boolean verbose) throws Exception {
        SwapEvent ev = (SwapEvent)colaEventos.poll(); //saco el evento de la cola

        currentTime = ev.getTime();
        int dimension = ev.getDimension();

        if (verbose) {
            out.println(ev.toString());
            out.println("Resolviendo evento simple");
        }

        // obtengo los OBJECT_ID relacionados al evento
        KineticSortedList kslTemp = listaKSL.get(dimension);

        int id0 = ev.getId0();
        Wrapper w0 = kslTemp.get(id0);
        int o0 = w0.getObjectId();

        int id1 = ev.getId1();
        Wrapper w1 = kslTemp.get(id1);
        int o1 = w1.getObjectId();

        // si un punto es minimo de un objeto y el otro es maximo,
        // estamos en presencia de: o se agrega un nuevo par de colisiones
        // o se elimina una colision existente
        if ((w0.isMaximum() && !w1.isMaximum()) || (!w0.isMaximum() && w1.isMaximum())) {
            // variable auxiliar
            Intersections aux = colisiones.get(dimension);

            Pair<Integer> parComparar = new PairInteger(o0, o1);

            if (aux.contains(parComparar)) {
                estadisticas.addSimpleEvent(currentTime, parComparar, dimension, "eliminar");
                aux.remove(parComparar); // si el par de objetos existe, se borra de la lista
                if (verbose) {
                    out.println("eliminar " + parComparar.toString() + " de la dimension " + dimension);
                }
            } else {
                estadisticas.addSimpleEvent(currentTime, parComparar, dimension, "agregar");
                aux.add(parComparar); // agregar el par
                if (verbose) {
                    out.println("agregar " + parComparar.toString() + " de la dimension " + dimension);
                }
            }

            if (verbose) {
                out.println(colisiones.toString());
            }
        }

        // antes de realizar el swap valido que esa zona este desordenada (solo como debug)
        if (verbose) {
            listaKSL.showZone(dimension, id0, id1, currentTime);
        }
        
        // realizar el swap
        listaKSL.swap(id0, id1, dimension);

        // actualizo el tiempo actual de la cola de eventos y calculo una nueva
        colaEventos.setCurrentTime(currentTime);

        // valido esa zona de la lista
        if (verbose){
            listaKSL.showZone(dimension, id0, id1, currentTime+1E-7);
            assert(listaKSL.validateZone(dimension, id0, id1, currentTime, 1E-7));
        }
        
        // lista de eventos de los vecinos (nuevos eventos)
        List<SubEventSwap> masEventos = kslTemp.calculateNeighborsEvents(id0, id1, currentTime);
        
        colaEventos.addAll(masEventos, dimension);
        masEventos.clear(); // limpiar la coleccion
    }

    /**
     *
     * @param colaEventos
     * @param listaKSL
     * @param colisiones
     * @param estadisticas
     * @param verbose
     * @throws Exception
     */
    public static void solveMultipleEvent(EventsQueue colaEventos, KSLList listaKSL, IntersectionsList colisiones,
            KineticStatistics estadisticas, boolean verbose) throws Exception {
        if (verbose) {
            out.println("Resolviendo evento multiple");
        }

        // se invalida la KSL respectiva y los eventos asociados a ella
        // obtener el rango de indices de los eventos a eliminar
        SwapEvent ev = (SwapEvent) colaEventos.peek();
        currentTime = ev.getTime();
        int dimension = ev.getDimension();

        // obtengo el rango de elementos a eliminar
        Pair<Integer> rangeToDelete = colaEventos.obtainRangeToDelete(currentTime, dimension);
        KineticSortedList ksl = listaKSL.get(dimension);
        
        if (verbose){
            out.println("rango de eventos a eliminar " + rangeToDelete.toString());
            ksl.showPositions(rangeToDelete, currentTime);
        }

        colaEventos.setCurrentTime(currentTime);
        estadisticas.addMultipleEvent(currentTime, obtainDeletionRange(ksl, rangeToDelete.getFirst(), rangeToDelete.getSecond()), dimension);

        // elimino los elementos de la cola de eventos que estan dentro del rango
        colaEventos.deleteMultipleEvents(currentTime, rangeToDelete.getFirst(), rangeToDelete.getSecond(), dimension);

        // Resuelvo en la ksl respectiva el orden de los objetos cineticos despues
        // del evento multiple. Para eso se evalua con un delta muy pequeño, el
        // nuevo orden.
        int minimo, maximo;
        
        minimo = (rangeToDelete.getFirst() == 0) ? 0 : rangeToDelete.getFirst() - 1;
        maximo = (rangeToDelete.getSecond() == ksl.size() - 1) ? ksl.size() - 1 : rangeToDelete.getSecond() + 1;

        ksl.insertionSort(minimo, maximo, currentTime, 1E-7);

        // reviso si la ksl actual es valida ahora
        assert(ksl.certificateValidation(currentTime));
        
        // actualizo la lista de colisiones en la dimension del evento multiple
        colisiones.set(dimension, ksl.obtainIntersections());

        // calculo los nuevos eventos entre los extremos del intervalo
        List<SubEventSwap> eventosNuevos = ksl.calculateNeighborsEvents(rangeToDelete.getFirst(), rangeToDelete.getSecond(), currentTime);

        // imprimo nueva lista de colisiones
        if (verbose) {
            out.println(colisiones.getColisiones());
        }

        // actualizo el tiempo en la cola de eventos
        colaEventos.setCurrentTime(currentTime);

        // agrego esos eventos nuevos a la cola de eventos
        colaEventos.addAll(eventosNuevos, dimension);

        eventosNuevos.clear(); // limpio la coleccion
    }

    /**
     * Del rango de elementos de la KSL, se deben obtener los objectId y esa
     * lista es la que se debe devolver!!!
     *
     * @param ksl
     * @param inicio posicion de inicio en la KSL
     * @param fin posicion de final en la KSL
     * @return lista de ObjectId
     */
    private static List<Pair<Integer>> obtainDeletionRange(KineticSortedList ksl, int inicio, int fin) {
        if (ksl != null) {
            List<Integer> resultadoInteger = new LinkedList<>();
            List<Pair<Integer>> listaParesCombinados = new LinkedList<>();

            if ((inicio >= 0) && (inicio < fin) && (fin < ksl.size())) {
                for (int i = inicio; i <= fin; i++) {
                    Integer objId = ksl.get(i).getObjectId();

                    if (!resultadoInteger.contains(objId)) {
                        resultadoInteger.add(objId);
                    }
                }

                for (int i = 0; i < resultadoInteger.size() - 1; i++) {
                    int obj0 = resultadoInteger.get(i);
                    for (int j = i + 1; j < resultadoInteger.size(); j++) {
                        int obj1 = resultadoInteger.get(j);

                        if (obj0 != obj1) {
                            listaParesCombinados.add(new PairInteger(min(obj0, obj1), max(obj0, obj1)));
                        }
                    }
                }

                return listaParesCombinados;
            }
        }
        return null;
    }
}
