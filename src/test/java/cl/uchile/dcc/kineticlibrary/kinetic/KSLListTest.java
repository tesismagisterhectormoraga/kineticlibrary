/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic;

import cl.uchile.dcc.kineticlibrary.kinetic.events.EventsObserver;
import cl.uchile.dcc.staticlibrary.primitives.Intersections;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class KSLListTest {

    /**
     *
     */
    public Wrapper p0x, 

    /**
     *
     */
    q0x, 

    /**
     *
     */
    p0y, 

    /**
     *
     */
    q0y, 

    /**
     *
     */
    p1x, 

    /**
     *
     */
    q1x, 

    /**
     *
     */
    p1y, 

    /**
     *
     */
    q1y, 

    /**
     *
     */
    p2x, 

    /**
     *
     */
    q2x, 

    /**
     *
     */
    p2y, 

    /**
     *
     */
    q2y;

    /**
     *
     */
    public KineticSortedList lista0, 

    /**
     *
     */
    lista1;

    /**
     *
     */
    public KSLList listaKSL;

    /**
     *
     */
    public KSLListTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        double t0 = 0.0;
        this.lista0 = new KineticSortedList();
        this.lista1 = new KineticSortedList();
        this.listaKSL = new KSLList(2);
        this.listaKSL.add(this.lista0);
        this.listaKSL.add(this.lista1);

        this.p0x = new Wrapper(new DimensionKinetic(1.0, -5.0), 0, false);
        this.q0x = new Wrapper(new DimensionKinetic(1.0, -1.0), 0, true);

        this.p0y = new Wrapper(new DimensionKinetic(-1.0, 1.0), 0, false);
        this.q0y = new Wrapper(new DimensionKinetic(-1.0, 2.0), 0, true);

        this.p1x = new Wrapper(new DimensionKinetic(-2.0, 1.0), 1, false);
        this.q1x = new Wrapper(new DimensionKinetic(-2.0, 3.0), 1, true);

        this.p1y = new Wrapper(new DimensionKinetic(0.0, -2.0), 1, false);
        this.q1y = new Wrapper(new DimensionKinetic(0.0, 2.0), 1, true);

        this.p2x = new Wrapper(new DimensionKinetic(-1.0, -3.0), 2, false);
        this.q2x = new Wrapper(new DimensionKinetic(-1.0, -1.0), 2, true);

        this.p2y = new Wrapper(new DimensionKinetic(2.0, -4.0), 2, false);
        this.q2y = new Wrapper(new DimensionKinetic(2.0, -2.0), 2, true);

        this.lista0.add(this.p0x);
        this.lista0.add(this.q0x);
        this.lista0.add(this.p1x);
        this.lista0.add(this.q1x);
        this.lista0.add(this.p2x);
        this.lista0.add(this.q2x);

        this.lista1.add(this.p0y);
        this.lista1.add(this.q0y);
        this.lista1.add(this.p1y);
        this.lista1.add(this.q1y);
        this.lista1.add(this.p2y);
        this.lista1.add(this.q2y);

        //sort de ambas listas en t0=0
        this.lista0.insertionSort(0, this.lista0.size(), t0, 1E-5);
        this.lista1.insertionSort(0, this.lista1.size(), t0, 1E-5);

    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.lista0.clear();
        this.lista1.clear();
        this.listaKSL.clear();
    }

    /**
     *
     */
    @Test
    public void testClear() {
        out.println("clear");
        KSLList instance = this.listaKSL;
        instance.clear();
        assertTrue(instance.isEmpty());
    }

    /**
     *
     */
    @Test
    public void testAdd() {
        out.println("add");
        KineticSortedList e = this.lista1;
        KSLList instance = new KSLList(1);
        boolean expResult = true;
        boolean result = instance.add(e);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testgetIntersections() {
        out.println("getIntersections");
        this.lista0.sort(1.0, 0.00001);
        this.lista1.sort(1.0, 0.00001);

        KSLList instance = new KSLList(1);
        instance.add(this.lista0);
        instance.add(this.lista1);

        Intersections expResult = new Intersections();
        expResult.add(new PairInteger(0, 2));
        expResult.add(new PairInteger(0, 1));
        Intersections result = instance.getIntersections();

        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testIsEmpty() {
        out.println("isEmpty");
        KSLList instance = new KSLList(1);
        boolean expResult = true;
        boolean result = instance.isEmpty();
        assertEquals(expResult, result);
        assertEquals(false, this.listaKSL.isEmpty());
    }

    /**
     *
     */
    @Test
    public void testSize() {
        out.println("size");
        int expResult = 2;
        int result = this.listaKSL.size();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testGet() {
        out.println("get");
        int i = 0;
        KineticSortedList expResult = this.lista0;
        KineticSortedList result = this.listaKSL.get(i);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testSet() {
        out.println("set");
        int i = 0;
        KineticSortedList KSL = this.listaKSL.get(0);
        KSLList instance = new KSLList(1);
        instance.add(new KineticSortedList());
        instance.set(i, KSL);
        assertFalse(instance.get(i).isEmpty());
    }

    /**
     *
     */
    @Test
    public void testGetKSLList() {
        out.println("getKSLList");
        List<KineticSortedList> expResult = new ArrayList<>();
        expResult.add(this.lista0);
        expResult.add(this.lista1);
        List<KineticSortedList> result = this.listaKSL.getKSLList();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testSetObserver() {
        out.println("setObserver");
        Observer o = new EventsObserver(this.listaKSL, null, null, null, false);
        this.listaKSL.setObserver(o);
    }

    /**
     *
     */
    @Test
    public void testSwap() {
        out.println("swap");
        int idx0 = 0;
        int idx1 = 1;
        int dimension = 0;
        KSLList instance = this.listaKSL;
        instance.swap(idx0, idx1, dimension);
        assertEquals(instance.get(0).get(0), this.p2x);
        assertEquals(instance.get(0).get(1), this.p0x);
    }

    /**
     * Test of obtenerIntersectionsPorDimension method, of class KSLList.
     */
    @Test
    public void testGetIntersectionsByDimension() {
        out.println("getIntersectionsByDimension");
        KSLList instance = this.listaKSL;
        List<Intersections> expResult = new ArrayList<>();
        Intersections interX = new Intersections();
        Intersections interY = new Intersections();
        interX.add(new PairInteger(0, 2));
        interY.add(new PairInteger(0, 1));
        interY.add(new PairInteger(1, 2));        
        
        expResult.add(interX);
        expResult.add(interY);
        List<Intersections> result = instance.getIntersectionsByDimension();
        assertEquals(expResult, result);
    }

    /**
     * Test of existeMultiplesEventos method, of class KSLList.
     */
    /*@Test
    public void testExisteMultiplesEventos() {
        System.out.println("existeMultiplesEventos");
        Evento ev = new EventoSwap(0, 1, 2.3, 0);
        KSLList instance = new KSLList();
        KineticSortedList listaTemp = new KineticSortedList();
        listaTemp.add(new Wrapper(new DimensionKinetic(2.0, -0.1), 0, false));
        listaTemp.add(new Wrapper(new DimensionKinetic(-2.0, 9.1), 1, true));
        listaTemp.add(new Wrapper(new DimensionKinetic(3.5, -3.55), 2, false));
        instance.add(listaTemp);
        
        boolean expResult = false; // por un asunto de decimales -_-
        boolean result = instance.existeMultiplesEventos(ev);
        assertEquals(expResult, result);
    }*/

    /**
     * Test of sort method, of class KSLList.
     */
    @Test
    public void testSort() {
        out.println("sort");
        double time = 1.1;
        double deltaTime = 0.0001;
        KSLList instance = this.listaKSL;
        KSLList expResult = new KSLList(1);
        expResult.add(this.lista0);
        expResult.add(this.lista1);        
        instance.sort(time, deltaTime);
        assertEquals(expResult.get(0), instance.get(0));
        assertEquals(expResult.get(1), instance.get(1));
    }
}
