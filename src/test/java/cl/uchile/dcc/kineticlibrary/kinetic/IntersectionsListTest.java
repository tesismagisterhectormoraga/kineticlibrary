/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic;

import cl.uchile.dcc.kineticlibrary.kinetic.events.EventsObserver;
import cl.uchile.dcc.staticlibrary.primitives.Intersections;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;
import static java.lang.System.nanoTime;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Observer;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class IntersectionsListTest {
    public Pair<Integer> par0, par1, par2, par3, par4;
    public IntersectionsList lista, listaColisiones;
    public Intersections listaX, listaY;

    /**
     *
     */
    public IntersectionsListTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.par0 = new PairInteger(0, 1);
        this.par1 = new PairInteger(0, 2);
        this.par2 = new PairInteger(1, 2);
        this.par3 = new PairInteger(2, 0);
        this.par4 = new PairInteger(1, 0);

        this.listaX = new Intersections();
        this.listaY = new Intersections();
        this.lista = new IntersectionsList();
        this.listaColisiones = new IntersectionsList();
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.listaX.clear();
        this.listaY.clear();
        this.lista.clear();
        this.listaColisiones.clear();
    }

    /**
     *
     */
    @Test
    public void testGetColisiones() {
        out.println("getColisiones");
        Intersections listaZ = new Intersections();

        this.listaX.add(this.par0);   // (0,1)
        this.listaX.add(this.par1);   // (0,2)

        this.listaY.add(this.par0);   // (0,1)
        this.listaY.add(this.par2);   // (1,2)
        this.listaY.add(this.par3);   // (2,0)

        listaZ.add(this.par4);   // (1,0)
        listaZ.add(this.par1);   // (0,2);       

        this.lista.add(this.listaX);
        this.lista.add(this.listaY);
        this.lista.add(listaZ);

        Intersections result = this.lista.getColisiones();
        Intersections expResult = new Intersections();
        expResult.addAll(this.listaX);

        assertTrue(result.size() == expResult.size());
        assertEquals(result.getCollisionsList(), expResult.getCollisionsList());
    }

    /**
     *
     */
    @Test
    public void testGetColisiones_long() {
        out.println("getColisiones_long");
        Random rnd =new Random();

        for (int i=0; i<=499; i++){
            this.listaX.add(new PairInteger(i, 999-i));
        }

        // ingreso casi toda la lista menos 4 objetos al azar
        while (this.listaY.size() < 495){
            int i=rnd.nextInt(500);
            this.listaY.add(new PairInteger(i, 999-i));
        }
        
        this.lista.add(this.listaX);
        this.lista.add(this.listaY);

        long t0 = nanoTime();
        Intersections result = this.lista.getColisiones();
        long t1 = nanoTime();
        out.println('\u0394'+"t="+(t1-t0));
        Intersections expResult = this.listaY;

        assertTrue(result.size() == expResult.size());
        assertEquals(result.getCollisionsList(),expResult.getCollisionsList());
    }    

    /**
     *
     */
    @Test
    public void testGetColisiones_long_2() {
        out.println("getColisiones_long_2");
        Intersections listaZ = new Intersections();
        Random rnd =new Random();
        
        for (int i=0; i<499; i++){
            this.listaX.add(new PairInteger(i, 999-i));
            this.listaY.add(new PairInteger(i, 999-i));
        }
        
        // elimino 5 datos al azar
        for (int i = 0; i < 5; i++) {
            int num = rnd.nextInt(this.listaY.size());
            int j=0;
            
            Pair<Integer> next = null;
            for (Iterator<Pair<Integer>> iterator = this.listaY.iterator(); j<num;) {
                next = iterator.next();
                
                j++;
            }
            
            listaZ.add(next);
            this.listaY.remove(next);
        }
        
        this.lista.add(this.listaX);
        this.lista.add(this.listaY);

        long t0 = nanoTime();
        Intersections result = this.lista.getColisiones();
        long t1 = nanoTime();
        out.println('\u0394'+"t="+(t1-t0));
        Intersections expResult = this.listaY;

        assertTrue(result.size() == expResult.size());
        assertEquals(result.getCollisionsList(),expResult.getCollisionsList());

        this.lista.clear();
        this.lista.add(this.listaX);
        this.lista.add(listaZ);

        t0 = nanoTime();        
        result = this.lista.getColisiones();
        t1 = nanoTime();
        out.println('\u0394'+"t="+(t1-t0));
        
        assertTrue(result.size()==5);
        assertEquals(listaZ, result);
    }    
    
    /**
     *
     */
    @Test
    public void testClear() {
        out.println("clear");
        IntersectionsList instance = this.lista;
        instance.clear();
        assertTrue(instance.size() == 0);
    }

    /**
     *
     */
    @Test
    public void testAdd() {
        out.println("add");
        Intersections e = new Intersections();
        e.add(this.par0);
        IntersectionsList instance = new IntersectionsList();
        boolean expResult = true;
        boolean result = instance.add(e);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testSetObserver() {
        out.println("setObserver");
        Observer o = new EventsObserver(null, this.listaColisiones, null, null, false);
        IntersectionsList instance = new IntersectionsList();
        instance.setObserver(o);
    }

    /**
     *
     */
    @Test
    public void testGet() {
        out.println("get");
        int dimension = 0;

        this.listaX.add(this.par0);   // (0,1)
        this.listaX.add(this.par1);   // (0,2)

        this.listaY.add(this.par0);   // (0,1)
        this.listaY.add(this.par2);   // (1,2)
        this.listaY.add(this.par3);   // (2,0)

        this.lista.add(this.listaX);
        this.lista.add(this.listaY);

        Intersections result = this.lista.get(dimension);
        Intersections expResult = new Intersections();
        expResult.addAll(this.listaX);

        assertTrue(result.size() == expResult.size());
        assertEquals(result.getCollisionsList(),expResult.getCollisionsList());
    }

    /**
     *
     */
    @Test
    public void testSize() {
        out.println("size");
        IntersectionsList instance = new IntersectionsList();
        int expResult = 0;
        int result = instance.size();
        assertEquals(expResult, result);
    }

    /**
     * Test of addAll method, of class IntersectionsList.
     */
    @Test
    public void testAddAllLists() {
        out.println("addAllLists");
        List<Intersections> c = new ArrayList<>();
        c.add(this.listaX);
        c.add(this.listaY);
        
        IntersectionsList instance = new IntersectionsList();
        boolean expResult = true;
        instance.addAllLists(c);
        
        //assertEquals(expResult, result);
        assertEquals(this.listaX, instance.get(0));
        assertEquals(this.listaY, instance.get(1));
        
    }

    /**
     * Test of set method, of class IntersectionsList.
     */
    @Test
    public void testSet() {
        out.println("set");
        int i = 0;
        this.listaX.add(this.par0);
        this.listaX.add(this.par1);
        this.listaX.add(this.par2);
        this.listaX.add(this.par3);
        this.listaX.add(this.par4);

        IntersectionsList instance = new IntersectionsList();

        instance.add(this.listaY);
        instance.add(this.listaX);
        instance.set(i, this.listaX);
        
        Intersections temp = new Intersections();
        temp.add(this.par0);
        temp.add(this.par1);
        temp.add(this.par2);
        temp.add(this.par3);
        temp.add(this.par4);
        IntersectionsList expResult = new IntersectionsList();
        expResult.add(temp);
        
        assertEquals(expResult.get(0), instance.get(0));
    }

    /**
     * Test of toString method, of class IntersectionsList.
     */
    @Test
    public void testToString() {
        out.println("toString");

        Intersections listaZ = new Intersections();

        this.listaX.add(this.par0);   // (0,1)
        this.listaX.add(this.par1);   // (0,2)

        this.listaY.add(this.par0);   // (0,1)
        this.listaY.add(this.par2);   // (1,2)
        this.listaY.add(this.par3);   // (2,0)

        listaZ.add(this.par4);   // (1,0)
        listaZ.add(this.par1);   // (0,2);       

        this.lista.add(this.listaX);
        this.lista.add(this.listaY);
        this.lista.add(listaZ);

        String result = this.lista.getColisiones().toString();
        String expResult = "[(0,1), (0,2)]";
        
        assertEquals(expResult, result);
    }
}
