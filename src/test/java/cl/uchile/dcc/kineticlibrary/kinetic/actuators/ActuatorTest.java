/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.actuators;

import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import cl.uchile.dcc.kineticlibrary.kinetic.IntersectionsList;
import cl.uchile.dcc.kineticlibrary.kinetic.KSLList;
import cl.uchile.dcc.kineticlibrary.kinetic.KineticSortedList;
import cl.uchile.dcc.kineticlibrary.kinetic.Object2D;
import cl.uchile.dcc.kineticlibrary.kinetic.events.EventsObserver;
import cl.uchile.dcc.kineticlibrary.kinetic.events.EventsQueue;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class ActuatorTest {

    private EventsQueue colaEventos;
    private KSLList listaKSL;
    private IntersectionsList colisiones;
    private KineticStatistics estadisticas;

    /**
     *
     */
    public ActuatorTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        Object2D obj1, obj2, obj3;
        this.listaKSL = new KSLList(2);
        this.estadisticas = new KineticStatistics();

        List<Point2D> list1 = new ArrayList<>(), list2 = new ArrayList<>(), list3 = new ArrayList<>();

        list1.add(new Point2D(3, 1));
        list1.add(new Point2D(5, 5));
        list1.add(new Point2D(1, 4));

        list2.add(new Point2D(-4, -3));
        list2.add(new Point2D(-2, -3));
        list2.add(new Point2D(-3, -1));

        list3.add(new Point2D(2, -3.5));
        list3.add(new Point2D(5, -5));
        list3.add(new Point2D(5, -2));

        obj1 = new Object2D(0, list1, new PairDouble(-1.0, -1.0));
        obj2 = new Object2D(1, list2, new PairDouble(2.0, 1.0));
        obj3 = new Object2D(2, list3, new PairDouble(-2.0, 1.0));

        KineticSortedList kslx = new KineticSortedList();
        KineticSortedList ksly = new KineticSortedList();

        kslx.addAll(obj1.getWrappers(0));
        kslx.addAll(obj2.getWrappers(0));
        kslx.addAll(obj3.getWrappers(0));
        kslx.sort(0, 0.00001);

        ksly.addAll(obj1.getWrappers(1));
        ksly.addAll(obj2.getWrappers(1));
        ksly.addAll(obj3.getWrappers(1));
        ksly.sort(0, 0.00001);

        this.listaKSL.add(kslx);
        this.listaKSL.add(ksly);

        this.colaEventos = new EventsQueue(new PairDouble(0.0, 10.0), true);
        this.colisiones = new IntersectionsList();

        // inicializo el observer
        EventsObserver observador = new EventsObserver(this.listaKSL, this.colisiones, this.colaEventos, this.estadisticas, false);

        // agrego el observer a todos los objetos necesarios
        this.listaKSL.setObserver(observador);
        this.colaEventos.setObserver(observador);
        this.colisiones.setObserver(observador);
        this.estadisticas.setObserver(observador);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of inicializarEstructuras method, of class Actuador.
     * @throws java.lang.Exception
     */
    @Test
    public void testStructuresInitialization() throws Exception {
        out.println("structuresInitialization");

        Actuator.structuresInitialization(this.colaEventos, this.listaKSL, this.colisiones, this.estadisticas, false);
        assertTrue(!this.colaEventos.isEmpty());
        assertTrue(this.colisiones.size() != 0);
    }

    /**
     * Test of resolverEventoSimple method, of class Actuador.
     * @throws java.lang.Exception
     */
    @Test
    public void testSolveSimpleEvent() throws Exception {
        out.println("solveSimpleEvent");
        Actuator.structuresInitialization(this.colaEventos, this.listaKSL, this.colisiones, this.estadisticas, false);
        Actuator.solveMultipleEvent(this.colaEventos, this.listaKSL, this.colisiones, this.estadisticas, false); // resuelvo evento en t=1.0
        Actuator.solveSimpleEvent(this.colaEventos, this.listaKSL, this.colisiones, this.estadisticas, false);
    }

    /**
     * Test of resolverEventoMultiple method, of class Actuador.
     * @throws java.lang.Exception
     */
    @Test
    public void testSolveMultipleEvent() throws Exception {
        out.println("solveMultipleEvent");
        Actuator.structuresInitialization(this.colaEventos, this.listaKSL, this.colisiones, this.estadisticas, false);
        Actuator.solveMultipleEvent(this.colaEventos, this.listaKSL, this.colisiones, this.estadisticas, false);
    }

}
