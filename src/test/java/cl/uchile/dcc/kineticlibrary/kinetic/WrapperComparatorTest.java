/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic;

import cl.uchile.dcc.kineticlibrary.kinetic.comparators.WrapperComparatorWithDelta;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class WrapperComparatorTest {

    Wrapper i1, i2, i3;

    /**
     *
     */
    public WrapperComparatorTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.i1 = new Wrapper(new DimensionKinetic(1.0, -1.0), 0, true);
        this.i2 = new Wrapper(new DimensionKinetic(2.0, 3.5), 1, true);
        this.i3 = new Wrapper(new DimensionKinetic(-1.0, 1.5), 2, true);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testCompare() {
        out.println("compare");
        WrapperComparatorWithDelta instance1 = new WrapperComparatorWithDelta(2.0, 1E-5);
        WrapperComparatorWithDelta instance2 = new WrapperComparatorWithDelta(0.0, 1E-5);
        int expResult0 = -1, expResult1 = 1, expResult2 = 0;
        int expResult3 = -1, expResult4 = 1, expResult5 = 0;
        int result0 = instance1.compare(this.i1, this.i2);
        int result1 = instance1.compare(this.i1, this.i3);
        int result2 = instance1.compare(this.i1, this.i1);
        int result3 = instance2.compare(this.i1, this.i2);
        int result4 = instance2.compare(this.i3, this.i1);
        int result5 = instance2.compare(this.i2, this.i2);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
        assertEquals(expResult4, result4);
        assertEquals(expResult5, result5);
    }
}
