/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic;

import cl.uchile.dcc.kineticlibrary.kinetic.comparators.DimensionKineticComparator;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class DimensionKineticTest {

    DimensionKinetic p1, p2, p3;

    /**
     *
     */
    public DimensionKineticTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.p1 = new DimensionKinetic(2.0, 3.0);
        this.p2 = new DimensionKinetic(-2.5, 1.5);
        this.p3 = new DimensionKinetic(1.5, -2.0);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetPosition() {
        out.println("getPosition");
        double t = 1.0;
        DimensionKinetic instance = this.p2;
        double expResult = -1.0;
        double result = instance.getPosition(t);
        assertEquals(expResult, result, 0.0);
    }

    /**
     *
     */
    @Test
    public void testGetV() {
        out.println("getV");
        DimensionKinetic instance = this.p1;
        double expResult = 2.0;
        double result = instance.getV();
        assertEquals(expResult, result, 0.0);
    }

    /**
     *
     */
    @Test
    public void testSetV() {
        out.println("setV");
        double v = 1.0;
        DimensionKinetic instance = this.p1;
        instance.setV(v);
        assertTrue(instance.getV() == v);
    }

    /**
     *
     */
    @Test
    public void testGetK() {
        out.println("getK");
        DimensionKinetic instance = this.p2;
        double expResult = 1.5;
        double result = instance.getK();
        assertEquals(expResult, result, 0.0);
    }

    /**
     *
     */
    @Test
    public void testSetK() {
        out.println("setK");
        double k = 1.2;
        DimensionKinetic instance = this.p2;
        instance.setK(k);
        assertTrue(instance.getK() == k);
    }

    /**
     *
     */
    @Test
    public void testToString() {
        out.println("toString");
        DimensionKinetic instance = this.p3;
        String expResult = "(v=1.5, k=-2.0)";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        DimensionKinetic instance = new DimensionKinetic(-2.3, 1.7);
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult != result);
    }

    /**
     *
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj = new DimensionKinetic(2.3, 1.7);
        DimensionKinetic instance0 = new DimensionKinetic(2.3, 1.7);
        DimensionKinetic instance1 = new DimensionKinetic(-2.3, 1.7);
        DimensionKinetic instance2 = new DimensionKinetic(2.3, -1.7);
        boolean expResult0 = true, expResult1 = false, expResult2 = false;
        boolean result0 = instance0.equals(obj);
        boolean result1 = instance1.equals(obj);
        boolean result2 = instance2.equals(obj);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     *
     */
    @Test
    public void testGetIntersection() {
        out.println("getIntersection");
        DimensionKinetic other = new DimensionKinetic(1, 3);
        DimensionKinetic instance = new DimensionKinetic(3, -1);
        double expResult = 2.0;
        double result = instance.getIntersection(other);
        assertEquals(expResult, result, 0.0);
    }

    /**
     *
     */
    @Test
    public void testCompareTo() {
        out.println("compareTo");
        DimensionKinetic other = new DimensionKinetic(3, 5);
        DimensionKinetic instance = new DimensionKinetic(2, 6);
        DimensionKineticComparator dkc1 = new DimensionKineticComparator(0.95);
        DimensionKineticComparator dkc2 = new DimensionKineticComparator(1.0);
        DimensionKineticComparator dkc3 = new DimensionKineticComparator(1.05);

        int expResult0 = -1, expResult1 = 0, expResult2 = 1;
        int result2 = dkc1.compare(instance, other);
        int result1 = dkc2.compare(instance, other); //2t+6 vs 3t+5 (en t=1)
        int result0 = dkc3.compare(instance, other);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     *
     */
    /*@Test
    public void testGetBorderTime() {
        System.out.println("getBorderTime");
        double border0 = -10.0, border1 = 10.0;
        double expResult00 = -6.5, expResult01 = 3.5;
        double expResult10 = 4.6, expResult11 = -3.4;
        double expResult20 = -16.0 / 3.0, expResult21 = 8.0;
        double result00 = this.p1.getBorderTime(border0);
        double result01 = this.p1.getBorderTime(border1);
        double result10 = this.p2.getBorderTime(border0);
        double result11 = this.p2.getBorderTime(border1);
        double result20 = this.p3.getBorderTime(border0);
        double result21 = this.p3.getBorderTime(border1);

        assertEquals(expResult00, result00, 0.0);
        assertEquals(expResult01, result01, 0.0);
        assertEquals(expResult10, result10, 0.0);
        assertEquals(expResult11, result11, 0.0);
        assertEquals(expResult20, result20, 0.0);
        assertEquals(expResult21, result21, 0.0);
    }*/
}
