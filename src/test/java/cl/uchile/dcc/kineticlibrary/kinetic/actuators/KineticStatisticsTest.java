/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.actuators;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import static java.lang.System.out;
import java.nio.file.Path;
import static java.nio.file.Paths.get;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

/**
 *
 * @author hmoraga
 */
public class KineticStatisticsTest {
    private final KineticStatistics estadisticas = new KineticStatistics();
    private final KineticStatistics estadisticas1 = new KineticStatistics();

    /**
     *
     */
    @Rule
    public TemporaryFolder folder= new TemporaryFolder();
    
    /**
     *
     */
    public KineticStatisticsTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     * @throws IOException
     */
    @Before
    public void setUp() throws IOException {
        this.folder.create();
        
        this.estadisticas.setTotalArea(100);
        this.estadisticas.setInitialOcupatedArea(10);
        this.estadisticas.setSimulationInterval(new PairDouble(0.0, 10.0));

        this.estadisticas1.setTotalArea(100);
        this.estadisticas1.setInitialOcupatedArea(10);
        this.estadisticas1.setSimulationInterval(new PairDouble(0.0, 10.0));
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.folder.delete();
    }

    /**
     * Test of getTiempoTotalSimulado method, of class Statistics.
     */
    /**
     * Test of getInitialOcupatedArea method, of class Statistics.
     */
    @Test
    public void testGetInitialOcupatedArea() {
        out.println("getInitialOcupatedArea");
        KineticStatistics instance = this.estadisticas;
        double expResult = 10.0;
        double result = instance.getInitialOcupatedArea();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setInitialOcupatedArea method, of class Statistics.
     */
    @Test
    public void testSetAreaOcupadaInicial() {
        out.println("setInitialOcupatedArea");
        double areaOcupadaInicial = 80.0;
        KineticStatistics instance = this.estadisticas;
        instance.setInitialOcupatedArea(areaOcupadaInicial);
        assertEquals(areaOcupadaInicial, instance.getInitialOcupatedArea(), 0.0);
    }

    /**
     * Test of addSimpleEvent method, of class Statistics.
     */
    @Test
    public void testAddEventoSimple() {
        out.println("addSimpleEvent");
        Pair<Integer> par = new PairInteger(0, 1);
        KineticStatistics instance = this.estadisticas;
        instance.addSimpleEvent(0.1, par, 0, "agregar");

        List<String> expResult = new ArrayList<>();

        expResult.add("areaTotal:100.0");
        expResult.add("tiempoTotalSimulado:10.0");
        expResult.add("areaOcupadaInicial:10.0");
        expResult.add("eventosSimples:1");
        expResult.add("eventosMultiples:0");
        expResult.add("0.1, [(0,1),X]+"); // tiempo, lista de pares
        List<String> listaTexto = instance.getText();
        
        assertEquals(expResult, listaTexto);
    }

    /**
     * Test of addMultipleEvent method, of class Statistics.
     */
    @Test
    public void testAddEventoMultiple() {
        out.println("addMultipleEvent");
        List<Pair<Integer>> objectsIdList = new ArrayList<>();
        objectsIdList.add(new PairInteger(0, 1));
        objectsIdList.add(new PairInteger(0, 2));
        objectsIdList.add(new PairInteger(1, 2));

        KineticStatistics instance = this.estadisticas1;
        instance.addMultipleEvent(0.1, objectsIdList, 0);

        List<String> expResult = new ArrayList<>();

        expResult.add("areaTotal:100.0");
        expResult.add("tiempoTotalSimulado:10.0");
        expResult.add("areaOcupadaInicial:10.0");
        expResult.add("eventosSimples:0");
        expResult.add("eventosMultiples:1");
        expResult.add("0.1, [(0,1), (0,2), (1,2)],X");
        List<String> listaAux = instance.getText();

        assertEquals(expResult, listaAux);
    }

    /**
     * Test of getText method, of class Statistics.
     */
    @Test
    public void testGetText() {
        out.println("getText");
        KineticStatistics instance = this.estadisticas;

        List<String> expResult = new ArrayList<>();
        expResult.add("areaTotal:100.0");
        expResult.add("tiempoTotalSimulado:10.0");
        expResult.add("areaOcupadaInicial:10.0");
        expResult.add("eventosSimples:0");
        expResult.add("eventosMultiples:0");

        List<String> result = instance.getText();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTotalArea method, of class KineticStatistics.
     */
    @Test
    public void testGetTotalArea() {
        out.println("getTotalArea");
        KineticStatistics instance = this.estadisticas;
        double expResult = 100.0;
        double result = instance.getTotalArea();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of setTotalArea method, of class KineticStatistics.
     */
    @Test
    public void testSetTotalArea() {
        out.println("setTotalArea");
        double areaTotal = 60.0;
        KineticStatistics instance = this.estadisticas;
        instance.setTotalArea(areaTotal);
        double result = instance.getTotalArea();
        assertEquals(areaTotal, result, 0.0);
    }

    /**
     * Test of getSimpleEvents method, of class KineticStatistics.
     */
    @Test
    public void testGetSimpleEvents() {
        out.println("getSimpleEvents");
        KineticStatistics instance = this.estadisticas1;
        instance.addSimpleEvent(0.1, new PairInteger(1,2), 0, "agregar");
        instance.addSimpleEvent(0.2, new PairInteger(2,3), 0, "agregar");
        instance.addSimpleEvent(0.3, new PairInteger(3,4), 0, "agregar");
        int expResult = 3;
        int result = instance.getSimpleEvents();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSimpleEvents method, of class KineticStatistics.
     */
    @Test
    public void testSetSimpleEvents() {
        out.println("setSimpleEvents");
        int eventosSimples = 3;
        KineticStatistics instance = this.estadisticas1;
        
        instance.setSimpleEvents(eventosSimples);
        int expResult = 3;
        int result = instance.getSimpleEvents();
        assertEquals(expResult, result);        
    }

    /**
     * Test of getMultipleEvents method, of class KineticStatistics.
     */
    @Test
    public void testGetEventosMultiples() {
        out.println("getMultipleEvents");
        List<Pair<Integer>> objectsIdList = new ArrayList<>();
        objectsIdList.add(new PairInteger(0, 1));
        objectsIdList.add(new PairInteger(0, 2));
        objectsIdList.add(new PairInteger(1, 2));

        List<Pair<Integer>> objectsIdList2 = new ArrayList<>();
        objectsIdList2.add(new PairInteger(1, 2));
        objectsIdList2.add(new PairInteger(1, 5));
        objectsIdList2.add(new PairInteger(2, 5));
        
        KineticStatistics instance = this.estadisticas1;
        instance.addMultipleEvent(0.1, objectsIdList, 0);
        instance.addMultipleEvent(0.2, objectsIdList2, 1);
        
        int expResult = 2;
        int result = instance.getMultipleEvents();
        assertEquals(expResult, result);
    }

    /**
     * Test of setMultipleEvents method, of class KineticStatistics.
     */
    @Test
    public void testSetMultipleEvents() {
        out.println("setMultipleEvents");
        KineticStatistics instance = this.estadisticas1;
        int eventosMultiples = 1;
        
        instance.setMultipleEvents(eventosMultiples);
        int result = instance.getMultipleEvents();
        assertEquals(eventosMultiples, result);
    }

    /**
     * Test of addAllEventoSimple method, of class KineticStatistics.
     */
    @Test
    public void testAddAllEventoSimple() {
        out.println("addAllEventoSimple");
        double time = 0.3;
        List<Pair<Integer>> listaPares = new ArrayList<>();
        listaPares.add(new PairInteger(0, 2));
        listaPares.add(new PairInteger(3, 7));
        listaPares.add(new PairInteger(1, 4));
        
        KineticStatistics instance = new KineticStatistics();
        instance.addAllSimpleEvents(time, listaPares);
        assertEquals(3, instance.getSimpleEvents());
    }

    /**
     * Test of hasMultipleEvents method, of class KineticStatistics.
     */
    @Test
    public void testHasMultipleEvents() {
        out.println("hasMultipleEvents");
        List<Pair<Integer>> objectsIdList = new ArrayList<>();
        objectsIdList.add(new PairInteger(0, 1));
        objectsIdList.add(new PairInteger(0, 2));
        objectsIdList.add(new PairInteger(1, 2));

        KineticStatistics instance = this.estadisticas1;
        instance.addMultipleEvent(0.1, objectsIdList, 0);

        boolean result = instance.hasMultipleEvents();
        assertTrue(result);
    }

    /**
     * Test of writeToFile method, of class KineticStatistics.
     * @throws java.lang.Exception
     */
    @Test
    public void testWriteToFile() throws Exception {
        out.println("writeToFile");
        File newFile = this.folder.newFile("archivoTemporal.txt");
        newFile.createNewFile();
        Path arch = get(newFile.toURI());
        KineticStatistics instance = this.estadisticas;
        instance.writeToFile(arch);
        
        List<String> expResult = new ArrayList<>();
        expResult.add("areaTotal:100.0");
        expResult.add("tiempoTotalSimulado:10.0");
        expResult.add("areaOcupadaInicial:10.0");        
        expResult.add("eventosSimples:0");
        expResult.add("eventosMultiples:0");
        
        // Read it from temp file
        int i=0;
        try (BufferedReader br = new BufferedReader(new FileReader(newFile))) {
            String line;
            while ((line = br.readLine()) != null) {
                assertEquals(expResult.get(i++), line);
            }
        }   
        
    }

    /**
     * Test of getSimulationInterval method, of class KineticStatistics.
     */
    @Test
    public void testGetSimulationInterval() {
        out.println("getSimulationInterval");
        KineticStatistics instance = this.estadisticas1;
        Pair<Double> expResult = new PairDouble(0, 10.0);
        Pair<Double> result = instance.getSimulationInterval();
        assertEquals(expResult, result);
    }

    /**
     * Test of setSimulationInterval method, of class KineticStatistics.
     */
    @Test
    public void testSetIntervaloSimulacion() {
        out.println("setSimulationInterval");
        Pair<Double> intervaloSimulacion = new PairDouble(0.0, 3.5);
        Pair<Double> expResult = new PairDouble(0.0, 3.5);
        KineticStatistics instance = this.estadisticas;
        instance.setSimulationInterval(intervaloSimulacion);
        Pair<Double> result = instance.getSimulationInterval();
        assertEquals(expResult, result);
    }

    /**
     * Test of setObserver method, of class KineticStatistics.
     */
    /*@Test
    public void testSetObserver() {
        System.out.println("setObserver");
        Observer o = null;
        KineticStatistics instance = new KineticStatistics();
        instance.setObserver(o);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/

    /**
     * Test of addListasIniciales method, of class KineticStatistics.
     */
    /*@Test
    public void testAddListasIniciales() {
        System.out.println("addListasIniciales");
        double actualTime = 0.0;
        ListaIntersecciones intersecciones = null;
        KineticStatistics instance = new KineticStatistics();
        instance.addListasIniciales(actualTime, intersecciones);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
}
