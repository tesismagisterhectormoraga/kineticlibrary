/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.events;

import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class SwapEventTest {

    SwapEvent ev1, ev2, ev3;

    /**
     *
     */
    public SwapEventTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.ev1 = new SwapEvent(1, 3, 2.5, 0);
        this.ev2 = new SwapEvent(1, 3, 2.5, 0);
        this.ev3 = new SwapEvent(1, 3, 2.3, 0);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetId0() {
        out.println("getId0");
        SwapEvent instance = this.ev2;
        int expResult = 1;
        int result = instance.getId0();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testGetId1() {
        out.println("getId1");
        SwapEvent instance = this.ev1;
        int expResult = 3;
        int result = instance.getId1();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testCompareTo() {
        out.println("compareTo");
        int expResult1 = 0, expResult2 = 1, expResult3 = -1;
        int result1 = this.ev1.compareTo(this.ev2), result2 = this.ev1.compareTo(this.ev3), result3 = this.ev3.compareTo(this.ev2);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
        assertEquals(expResult3, result3);
    }

    /**
     *
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        SwapEvent instance = this.ev1;
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult != result);
    }

    /**
     *
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj = this.ev1;

        SwapEvent instance1 = this.ev2, instance2 = this.ev3;
        boolean expResult1 = true, expResult2 = false;
        boolean result1 = instance1.equals(obj);
        boolean result2 = instance2.equals(obj);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     *
     */
    @Test
    public void testToString() {
        out.println("toString");
        SwapEvent instance = new SwapEvent(2, 3, 7.8, 0);
        String expResult = "EV(t=7.8)2_3X";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
}
