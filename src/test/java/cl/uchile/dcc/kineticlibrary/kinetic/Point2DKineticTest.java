/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic;

import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Point2DKineticTest {

    /**
     *
     */
    public Point2DKineticTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetPosition() {
        out.println("getPoint2D");
        double time = 1.0;
        Point2DKinetic instance = new Point2DKinetic(3.0, 1.0, 1.0, 5.0);
        Point2D expResult = new Point2D(4.0, 6.0);
        Point2D result = instance.getPosition(time);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testSetPosition() {
        out.println("setPosition");
        double time = 2.0;
        Point2DKinetic instance = new Point2DKinetic(3.0, 1.0, -1.0, 5.0);
        instance.setPosition(time);
        // el tiempo (0,0) desde que me movi a la nueva posicion
        assertEquals(instance.getPosition(0.0), new Point2D(7.0, 3.0));
    }

    /**
     *
     */
    @Test
    public void testSetVelocidad() {
        out.println("setVelocidad");
        Pair<Double> velocidades = new PairDouble(1.5, -2.5);
        Point2DKinetic instance = new Point2DKinetic(3.0, 1.0, -1.0, 5.0);
        instance.setVelocidad(velocidades);
        assertEquals(instance.getVelocidad(), velocidades);
    }

    /**
     *
     */
    @Test
    public void testGetVelocidad() {
        out.println("getVelocidad");
        Point2DKinetic instance = new Point2DKinetic(3.0, 1.0, -1.0, 5.0);
        Pair<Double> expResult = new PairDouble(3.0, -1.0);
        Pair<Double> result = instance.getVelocidad();
        assertEquals(expResult, result);
    }

}
