/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.oldfiles;

import cl.uchile.dcc.kineticlibrary.kinetic.DimensionKinetic;
import cl.uchile.dcc.kineticlibrary.kinetic.Interval;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class IntervalListTest {

    Interval i0, i1, i2, i3, i4;
    IntervalList lista;

    /**
     *
     */
    public IntervalListTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.i0 = new Interval(new DimensionKinetic(2.0, -10), new DimensionKinetic(2.0, -6));
        this.i1 = new Interval(new DimensionKinetic(-1.0, -10), new DimensionKinetic(-1.0, -5));
        this.i2 = new Interval(new DimensionKinetic(1.5, -5), new DimensionKinetic(1.5, -2));
        this.i3 = new Interval(new DimensionKinetic(-1.5, -5), new DimensionKinetic(-1.5, -2));
        this.i4 = new Interval(new DimensionKinetic(-2, -7), new DimensionKinetic(-2, -1));

        this.lista = new IntervalList();
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.lista.clear();
    }

    /**
     *
     */
    @Test
    public void testInsertionSort() {
        out.println("insertionSort");
        this.lista.add(this.i0);
        this.lista.add(this.i1);
        this.lista.add(this.i2);
        this.lista.add(this.i3);
        this.lista.add(this.i4);
        this.lista.insertionSort(0.0);

        assertEquals(this.lista.get(0), this.i0);
        assertEquals(this.lista.get(1), this.i1);
        assertEquals(this.lista.get(2), this.i4);
        assertEquals(this.lista.get(3), this.i2);
        assertEquals(this.lista.get(4), this.i3);
    }
}
