/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic;

import cl.uchile.dcc.staticlibrary.poligons.AnyPolygon2D;
import cl.uchile.dcc.staticlibrary.poligons.Polygon2D;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class Object2DTest {

    /**
     *
     */
    public Object2D byPair, 

    /**
     *
     */
    byPolygon2D;

    /**
     *
     */
    public Polygon2D poligono;

    /**
     *
     */
    public List<Point2D> listaPuntos;

    /**
     *
     */
    public PairDouble velocidades;

    /**
     *
     */
    public List<PairDouble> dimensiones;
    private List<Point2D> puntos;

    /**
     *
     */
    public Object2DTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.puntos = new ArrayList<>();
        this.puntos.add(new Point2D(-2, -2));
        this.puntos.add(new Point2D(2, -2));
        this.puntos.add(new Point2D(2, 2));
        this.puntos.add(new Point2D(-2, 2));

        this.listaPuntos = new ArrayList<>(4);
        this.listaPuntos.add(new Point2D(-2.0, -2.0));
        this.listaPuntos.add(new Point2D(2.0, -2.0));
        this.listaPuntos.add(new Point2D(2.0, 2.0));
        this.listaPuntos.add(new Point2D(-2.0, 2.0));

        this.poligono = new AnyPolygon2D(this.puntos, false);
        this.dimensiones = new ArrayList<>(2);
        this.dimensiones.add(new PairDouble(-5.0, 5.0));
        this.dimensiones.add(new PairDouble(-5.0, 5.0));
        this.velocidades = new PairDouble(3.0, -1.0);

        this.byPolygon2D = new Object2D(0, this.poligono.getVertices(), this.velocidades, 0);
        this.byPair = new Object2D(1, this.listaPuntos, this.velocidades);
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.listaPuntos.clear();
    }

    /**
     *
     */
    @Test
    public void testGetListaPuntos() {
        out.println("getListaPuntos");
        Object2D instance0 = this.byPair;
        Object2D instance1 = this.byPolygon2D;

        List<Point2D> expResult0 = new ArrayList<>();
        expResult0.add(new Point2D(-2.0, -2.0));
        expResult0.add(new Point2D(2.0, -2.0));
        expResult0.add(new Point2D(2.0, 2.0));
        expResult0.add(new Point2D(-2.0, 2.0));

        List<Point2D> result0 = instance0.getListaPuntos();
        List<Point2D> result1 = instance1.getListaPuntos();
        assertEquals(expResult0, result0);
        assertEquals(expResult0, result1);
    }

    /**
     *
     */
    @Test
    public void testGetVelocidad() {
        out.println("getVelocidad");
        Object2D instance0 = this.byPair, instance1 = this.byPolygon2D;
        Pair<Double> expResult = new PairDouble(3.0, -1.0);
        Pair<Double> result0 = instance0.getVelocidad();
        Pair<Double> result1 = instance1.getVelocidad();
        assertEquals(expResult, result0);
        assertEquals(expResult, result1);
    }

    /**
     *
     */
    @Test
    public void testSetVelocidad() {
        out.println("setVelocidad");
        Pair<Double> velocidad = new PairDouble(-3.0, 1.0);
        Object2D instance = this.byPolygon2D;
        instance.setVelocidad(velocidad);
        assertEquals(instance.getVelocidad(), velocidad);
    }

    /* @Test
    public void testGetTimeBorderEvent() {
        System.out.println("getTimeBorderEvent");
        int dimension0 = 0, dimension1 = 1;
        List<PairDouble> limites = new ArrayList<>(2);
        limites.add(new Pair<>(-6.0, 6.0));
        limites.add(new Pair<>(-6.0, 6.0));
        Object2D instance = byPair;
        double expResult0 = 4.0/3.0, expResult1 = 4.0;
        double result0 = instance.getTimeBorder(limites, dimension0);
        double result1 = instance.getTimeBorder(limites, dimension1);
        
        assertEquals(expResult0, result0, 0.0);
        assertEquals(expResult1, result1, 0.0);        
    } */

    /**
     *
     */

    @Test
    public void testSetSpeed() {
        out.println("setSpeed");
        double vx = -3.0, vy = 1.0;
        Object2D instance = this.byPolygon2D;

        instance.setVelocidad(new PairDouble(vx, vy));
        assertEquals(instance.getVelocidad(), new PairDouble(-3.0, 1.0));
    }

    /**
     *
     */
    @Test
    public void testGetObjId() {
        out.println("getObjId");
        Object2D instance0 = this.byPair, instance1 = this.byPolygon2D;
        int result0 = instance0.getObjId();
        int result1 = instance1.getObjId();
        assertEquals(1, result0);
        assertEquals(0, result1);
    }

    /*    @Test
    public void testExcedeBorde() {
        System.out.println("excedeBorde");
        double timeStep = 0.1;
        Object2D instance0 = byPair, instance1 = byPolygon2D;
        boolean expResult0 = false, expResult1 = false;
        boolean result0 = instance0.excedeBorde(dimensiones, timeStep);
        boolean result1 = instance1.excedeBorde(dimensiones, timeStep);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);        
    } */

 /*    @Test
    public void testGetTimeBorder() {
        System.out.println("getTimeBorder");
        int dim0 = 0, dim1 = 1;
        Object2D instance0 = byPolygon2D, instance1 = byPair;
        double expResult0 = 1.0, expResult1 = 3.0;
        double result0 = instance0.getTimeBorder(dimensiones, dim0);
        double result1 = instance1.getTimeBorder(dimensiones, dim1);        
        assertEquals(expResult0, result0, 0.0);
        assertEquals(expResult1, result1, 0.0);
    } */

    /**
     *
     */

    @Test
    public void testGetWrappers() {
        out.println("getWrappers");
        int dim0 = 0, dim1 = 1;
        Object2D instance = this.byPair;
        List<Wrapper> expResult0 = new ArrayList<>(), expResult1 = new ArrayList<>();
        Wrapper px = new Wrapper(new DimensionKinetic(3.0, -2), 1, false);
        Wrapper qx = new Wrapper(new DimensionKinetic(3.0, 2), 1, true);
        Wrapper py = new Wrapper(new DimensionKinetic(-1.0, -2), 1, false);
        Wrapper qy = new Wrapper(new DimensionKinetic(-1.0, 2), 1, true);
        expResult0.add(px);
        expResult0.add(qx);
        expResult1.add(py);
        expResult1.add(qy);

        List<Wrapper> result0 = instance.getWrappers(dim0);
        List<Wrapper> result1 = instance.getWrappers(dim1);
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /*    @Test
    public void testSetPosition() {
        System.out.println("setPosition");
        double time = 2.0;
        Object2D instance = byPair;
        instance.setPosition(dimensiones, time);
        List<Point2D> ptos = instance.getListaPuntos();
        assertEquals(ptos, listaPuntos);
    }

    @Test
    public void testSetPosition1() {
        System.out.println("setPosition_1");
        double time = 1.0;
        Object2D instance = byPolygon2D;
        List<Point2D> expResult = new ArrayList<>();
        expResult.add(new Point2D(1.0,-3.0));
        expResult.add(new Point2D(5.0,-3.0));
        expResult.add(new Point2D(5.0,1.0));
        expResult.add(new Point2D(1.0,1.0));
        
        instance.setPosition(dimensiones, time);
        List<Point2D> result = instance.getListaPuntos();
        assertEquals(expResult, result);
    }

    @Test
    public void testSetPosition2() {
        System.out.println("setPosition_2");
        double time = 3.0;
        Object2D instance = byPolygon2D;
        instance.setPosition(dimensiones, time);
        List<Point2D> listaPtos = new ArrayList<>();
        listaPtos.add(new Point2D(-5.0,-5.0));
        listaPtos.add(new Point2D(-1.0,-5.0));
        listaPtos.add(new Point2D(-1.0,-1.0));
        listaPtos.add(new Point2D(-5.0,-1.0));
        assertEquals(instance.getListaPuntos(), listaPtos);
    } */

    /**
     *
     */

    @Test
    public void testGetPosition() {
        out.println("getPosition");
        double time0 = 3.0, time1 = 5.0;
        Object2D instance = new Object2D(0, this.poligono.getVertices(), this.velocidades, 0.0);

        // comparando en t=3.0
        List<Point2D> result0 = instance.getPosition(time0);
        List<Point2D> expResult0 = new ArrayList<>();

        expResult0.add(new Point2D(7, -5));
        expResult0.add(new Point2D(11, -5));
        expResult0.add(new Point2D(11, -1));
        expResult0.add(new Point2D(7, -1));

        assertEquals(expResult0, result0);

        // comparando en t=5.0
        // el problema aqui es que asume su t_actual como el ultimo
        // ingresado, el cual me causa problemas en la actualizacion
        // se debe pedir un setTiempoActual manual!!!
        List<Point2D> result1 = instance.getPosition(time1);
        List<Point2D> expResult1 = new ArrayList<>();

        expResult1.add(new Point2D(13, -7));
        expResult1.add(new Point2D(17, -7));
        expResult1.add(new Point2D(17, -3));
        expResult1.add(new Point2D(13, -3));

        assertEquals(expResult1, result1);
    }

    /*@Test
    public void testUpdatePosition() throws CloneNotSupportedException {
        System.out.println("updatePosition");
        double time0 = 3.0, time1= 5.0;
        Object2D instance = new Object2D(0, listaPuntos, velocidades);
        
        instance. updatePosition(time0);
        List<Point2D> result0 = instance.getListaPuntos();
        List<Point2D> expResult0 = new ArrayList<>();
        expResult0.add(new Point2D(7, -5));
        expResult0.add(new Point2D(11, -5));
        expResult0.add(new Point2D(11, -1));
        expResult0.add(new Point2D(7, -1));

        assertEquals(expResult0, result0);
        
        instance.updatePosition(time1);
        List<Point2D> result1 = instance.getListaPuntos();
        List<Point2D> expResult1 = new ArrayList<>();
        expResult1.add(new Point2D(13, -7));
        expResult1.add(new Point2D(17, -7));
        expResult1.add(new Point2D(17, -3));
        expResult1.add(new Point2D(13, -3));
                
        assertEquals(expResult1, result1);        
    }*/
    /**
     * Test of close method, of class Object2D.
     */
    @Test
    public void testClose() {
        out.println("close");
        Object2D instance = this.byPair;
        instance.close();

        assertTrue(this.byPair.getListaPuntos().isEmpty());
        assertNull(this.byPair.getVelocidad());
    }

    /**
     * Test of setActualTime method, of class Object2D.
     */
    @Test
    public void testSetActualTime() {
        out.println("setActualTime");
        double time = 1.0;
        Object2D instance = new Object2D(0, this.listaPuntos, this.velocidades);
        instance.setActualTime(0.9);
        List<Point2D> expResult = new ArrayList<>(4);
        expResult.add(new Point2D(-2+0.3, -2-0.1));
        expResult.add(new Point2D(2+0.3, -2-0.1));
        expResult.add(new Point2D(2+0.3, 2-0.1));
        expResult.add(new Point2D(-2+0.3, 2-0.1));
        List<Point2D> result = instance.getPosition(time);
        
        for (int i = 0; i < result.size(); i++) {
            assertEquals(expResult.get(i).getX(), result.get(i).getX(), 0.00001);
            assertEquals(expResult.get(i).getY(), result.get(i).getY(), 0.00001);
        }
    }

    /**
     * Test of getArea method, of class Object2D.
     */
    @Test
    public void testGetArea() {
        out.println("getArea");
        int id = 1;
        double time = 1.0;
        List<Point2D> listaPtosTemp = new ArrayList<>();
        listaPtosTemp.add(new Point2D(2, -1));
        listaPtosTemp.add(new Point2D(4, 1));
        listaPtosTemp.add(new Point2D(2, 3));
        listaPtosTemp.add(new Point2D(0, 1));
        
        Pair<Double> velocidad = new PairDouble(1.5, 0.0);
        
        Object2D instance = new Object2D(id, listaPtosTemp, velocidad, time);
        double expResult = 8.0;
        double result = instance.getArea();
        assertEquals(expResult, result, 0.00001);
    }
}
