/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic;

import cl.uchile.dcc.kineticlibrary.kinetic.events.SubEventSwap;
import cl.uchile.dcc.staticlibrary.primitives.Intersections;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class KineticSortedListTest {

    KineticSortedList lista;

    /**
     *
     */
    public KineticSortedListTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.lista = new KineticSortedList();
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.lista.clear();
    }

    /**
     *
     */
    @Test
    public void testAddAll() {
        out.println("addAll");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);

        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);

        KineticSortedList instance = this.lista;
        boolean expResult = true;
        boolean result = instance.addAll(listaPuntos);
        assertEquals(listaPuntos.get(0), instance.get(0));
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testAdd() {
        out.println("add");
        Wrapper puntoCinetico = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, false);
        KineticSortedList instance = this.lista;
        boolean expResult = true;
        boolean result = instance.add(puntoCinetico);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testValidarCertificado_1() {
        out.println("validarCertificado sin delta");
        double time = 0.0;
        List<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper q0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p1 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper q1 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);

        listaPuntos.add(q0);
        listaPuntos.add(p0);
        listaPuntos.add(q1);
        listaPuntos.add(p1);

        KineticSortedList instance = this.lista;
        instance.addAll(listaPuntos);
        boolean result0 = instance.certificateValidation(time);
        boolean expResult0 = false;
        instance.sort(time);
        boolean result1 = instance.certificateValidation(time);
        boolean expResult1 = true;
        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     *
     */
    @Test
    public void testInsertionSort() {
        out.println("insertionSort");
        double time = 0.0;
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);

        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);

        KineticSortedList instance = this.lista;
        instance.addAll(listaPuntos);

        instance.insertionSort(0, instance.size(), time, 0.00001);
        assertTrue(instance.get(0) == p1);
        assertTrue(instance.get(1) == p2);
        assertTrue(instance.get(2) == p0);
        assertTrue(instance.get(3) == p3);
    }

    /**
     *
     */
    @Test
    public void testSwap() {
        out.println("swap");
        double time = 0.0;

        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);

        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);

        KineticSortedList instance = this.lista;
        instance.addAll(listaPuntos);

        instance.sort(time, 0.00001);
        instance.swap(0, 1);

        assertEquals(instance.get(0), p2);
        assertEquals(instance.get(1), p1);
    }

    /**
     *
     */
    @Test
    public void testCalcularEventosVecinos() {
        out.println("calcularEventosVecinos");
        double time = 0.0;

        List<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);

        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);

        KineticSortedList instance = this.lista;
        instance.addAll(listaPuntos);
        instance.sort(time, 1E-5);

        List<SubEventSwap> listaEventos = instance.calculateNeighborsEvents(1, 2, time+1E-5);

        assertTrue(listaEventos.isEmpty());
    }

    /**
     *
     */
    @Test
    public void testCalcularEventos() {
        out.println("calcularEventos");
        double time = 0.0;
        boolean include = false;
        KineticSortedList instance = new KineticSortedList();
        instance.add(new Wrapper(new DimensionKinetic(1.0, 2.0), 0, false));
        instance.add(new Wrapper(new DimensionKinetic(1.0, 5.0), 0, true));
        instance.add(new Wrapper(new DimensionKinetic(0.5, 3.0), 1, false));
        instance.add(new Wrapper(new DimensionKinetic(0.5, 6.0), 1, true));
        instance.add(new Wrapper(new DimensionKinetic(-2.0, 4.0), 2, false));
        instance.add(new Wrapper(new DimensionKinetic(-2.0, 7.0), 2, true));

        List<SubEventSwap> expResult = new ArrayList<>();
        SubEventSwap ev0, ev1, ev2, ev3;

        ev0 = new SubEventSwap(2.0, 0, 1);
        ev1 = new SubEventSwap(2.0 / 5.0, 1, 2);
        ev2 = new SubEventSwap(2.0, 3, 4);
        ev3 = new SubEventSwap(2.0 / 5.0, 4, 5);

        expResult.add(ev0);
        expResult.add(ev1);
        expResult.add(ev2);
        expResult.add(ev3);

        instance.insertionSort(0, instance.size() - 1, time, 0.00001);
        List<SubEventSwap> result = instance.calculateSwapEvents(time, include);

        int i = 0;
        for (SubEventSwap evento : result) {
            assertEquals(evento, expResult.get(i++));
        }
    }

    /*@Test
    public void testTestIfMultiplesEventos() {
        System.out.println("testIfMultiplesEventos");
        double time = 0.0;

        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(-1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 3.0), 1, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.9), 2, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(-2.0, 3.0), 3, true);

        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);

        KineticSortedList instance = lista;
        instance.addAll(listaPuntos);
        EventoSwap ev = new EventoSwap(1, 2, 0, 0);
        assertTrue(instance.testIfMultiplesEventos(ev));
    }*/

    /**
     *
     */


    @Test
    public void testClear() {
        out.println("clear");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);

        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);

        KineticSortedList instance = this.lista;
        instance.addAll(listaPuntos);
        assertTrue(instance.size() == 4);
        instance.clear();
        assertTrue(instance.isEmpty());
    }

    /**
     *
     */
    @Test
    public void testGet() {
        out.println("get");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);

        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);

        KineticSortedList instance = this.lista;
        instance.addAll(listaPuntos);

        assertEquals(instance.get(0), p0);
        assertEquals(instance.get(1), p3);
    }

    /**
     *
     */
    @Test
    public void testSize() {
        out.println("size");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);

        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);

        KineticSortedList instance = this.lista;
        instance.addAll(listaPuntos);
        int expResult = 4;

        int result = instance.size();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testObtainIntersections() {
        out.println("obtainIntersections");
        List<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p2 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper p3 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);

        listaPuntos.add(p0);
        listaPuntos.add(p3);
        listaPuntos.add(p1);
        listaPuntos.add(p2);

        KineticSortedList instance = this.lista;
        instance.addAll(listaPuntos);
        instance.sort(0.0, 0.00001);

        Intersections expResult = new Intersections();
        expResult.add(new PairInteger(0, 1));

        Intersections result = instance.obtainIntersections();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testCalcularEventosVecinos_int_int() {
        out.println("calcularEventosVecinos");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper q0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper q1 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);

        listaPuntos.add(p0);
        listaPuntos.add(q0);
        listaPuntos.add(p1);
        listaPuntos.add(q1);

        KineticSortedList instance = this.lista;
        instance.addAll(listaPuntos);
        instance.insertionSort(0, instance.size() - 1, 0.0, 1E-5);

        List<SubEventSwap> expResult0 = new ArrayList<>();
        expResult0.add(new SubEventSwap(1.0, 1, 2));
        List<SubEventSwap> expResult1 = new ArrayList<>();
        List<SubEventSwap> expResult2 = new ArrayList<>();
        expResult2.add(new SubEventSwap(1.0, 1, 2));

        List<SubEventSwap> result0 = instance.calculateNeighborsEvents(0, 1, 1E-5);
        List<SubEventSwap> result1 = instance.calculateNeighborsEvents(1, 2, 1E-5);
        List<SubEventSwap> result2 = instance.calculateNeighborsEvents(2, 3, 1E-5);

        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     *
     */
    /*@Test
    public void testCalcularEventosVecinos_int() {
        System.out.println("calcularEventosVecinos");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p1 = new Wrapper(new DimensionKinetic(-1.0, 3.0), 1, false);
        Wrapper q0 = new Wrapper(new DimensionKinetic(1.0, 2.0), 0, true);
        Wrapper q1 = new Wrapper(new DimensionKinetic(-1.0, 4.0), 1, true);

        listaPuntos.add(p0);
        listaPuntos.add(p1);
        listaPuntos.add(q0);
        listaPuntos.add(q1);

        this.lista.addAll(listaPuntos);
        this.lista.insertionSort(0, this.lista.size() - 1, 0.0, 0.00001);

        int pos = 1;
        List<SubEventSwap> expResult = new ArrayList<>();
        expResult.add(new SubEventSwap(Double.POSITIVE_INFINITY, 0, 1));
        expResult.add(new SubEventSwap(0.5, 1, 2));
        List<SubEventSwap> result = this.lista.calculateNeighborsEvents(pos);
        assertEquals(expResult, result);
    }*/

    /**
     *
     */
    @Test
    public void testCalcularEventosSwap_double_boolean() {
        out.println("calcularEventosSwap");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper q1 = new Wrapper(new DimensionKinetic(-1.0, 3.5), 1, true);
        Wrapper q0 = new Wrapper(new DimensionKinetic(1.0, 2.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(-1.0, 1.5), 1, false);

        listaPuntos.add(p0);
        listaPuntos.add(q1);
        listaPuntos.add(q0);
        listaPuntos.add(p1);

        this.lista.addAll(listaPuntos);
        this.lista.sort(0.0, 0.00001);

        double time = 0.0;
        boolean include = false;

        List<SubEventSwap> expResult = new ArrayList<>();
        expResult.add(new SubEventSwap(0.25, 0, 1));
        expResult.add(new SubEventSwap(0.75, 2, 3));
        // son solo dos eventos porque no actualizamos la ksl,
        // son solo los eventos iniciales
        List<SubEventSwap> result = this.lista.calculateSwapEvents(time, include);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testCalcularEventosSwap_4args() {
        out.println("calcularEventosSwap");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper p1 = new Wrapper(new DimensionKinetic(-1.0, 3.0), 1, false);
        Wrapper q0 = new Wrapper(new DimensionKinetic(1.0, 2.0), 0, true);
        Wrapper q1 = new Wrapper(new DimensionKinetic(-1.0, 4.0), 1, true);

        listaPuntos.add(p0);
        listaPuntos.add(q0);
        listaPuntos.add(p1);
        listaPuntos.add(q1);

        KineticSortedList instance = this.lista;
        instance.addAll(listaPuntos);
        instance.insertionSort(0, instance.size() - 1, 0.5, 0.00001);

        int idInicio = 0;
        int idFinal = 3;
        double time = 1.1;
        boolean include = false;
        List<SubEventSwap> expResult = new ArrayList<>();
        List<SubEventSwap> result = instance.calculateSwapEvents(idInicio, idFinal, time, include);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testObtainRangeToDelete() {
        out.println("obtainRangeToDelete");
        List<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper q0 = new Wrapper(new DimensionKinetic(1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(2.0, 3.0), 1, false);
        Wrapper q1 = new Wrapper(new DimensionKinetic(2.0, 4.0), 1, true);
        Wrapper p2 = new Wrapper(new DimensionKinetic(1.5, 3.0), 2, false);
        Wrapper q2 = new Wrapper(new DimensionKinetic(1.5, 4.0), 2, true);

        listaPuntos.add(p0);
        listaPuntos.add(q0);
        listaPuntos.add(p1);
        listaPuntos.add(q1);
        listaPuntos.add(p2);
        listaPuntos.add(q2);

        KineticSortedList instance = this.lista;
        instance.addAll(listaPuntos);
        instance.insertionSort(0, instance.size(), 0.0, 1E-5);

        double time = 0.0;
        Pair<Integer> expResult = new PairInteger(1, 3);    //posiciones en la KSL ordenada
        Pair<Integer> result = instance.obtainRangeToDelete(time, time/1000);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testToString() {
        out.println("toString");
        LinkedList<Wrapper> listaPuntos = new LinkedList<>();
        Wrapper p0 = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, false);
        Wrapper q0 = new Wrapper(new DimensionKinetic(-1.0, 3.0), 0, true);
        Wrapper p1 = new Wrapper(new DimensionKinetic(2.0, 2.0), 1, false);
        Wrapper q1 = new Wrapper(new DimensionKinetic(-2.0, 4.0), 1, true);

        listaPuntos.add(p0);
        listaPuntos.add(q0);
        listaPuntos.add(p1);
        listaPuntos.add(q1);

        KineticSortedList instance = this.lista;
        instance.addAll(listaPuntos);
        instance.insertionSort(0, instance.size() - 1, 0.0, 0.00001);

        String expResult = "p0 p1 q0 q1 ";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testAdd_Object2D_int() {
        out.println("add");
        Pair<Double> velocidades = new PairDouble(1.0, -2.0);
        List<Point2D> listaPtos = new ArrayList<>();

        listaPtos.add(new Point2D(2.0, -1.0));
        listaPtos.add(new Point2D(3.0, 0.0));
        listaPtos.add(new Point2D(1.0, 1.0));
        listaPtos.add(new Point2D(0.0, 0.0));
        listaPtos.add(new Point2D(0.0, -2.0));
        listaPtos.add(new Point2D(1.0, -3.0));
        listaPtos.add(new Point2D(3.0, -2.0));

        Object2D objeto = new Object2D(0, listaPtos, velocidades);
        KineticSortedList instance0 = new KineticSortedList();
        KineticSortedList instance1 = new KineticSortedList();
        boolean expResult = true;
        boolean result0 = instance0.add(objeto, 0);
        boolean result1 = instance1.add(objeto, 1);
        assertEquals(expResult, result0);
        assertEquals(expResult, result1);
        assertTrue(instance0.size() == 2);
        assertTrue(instance1.size() == 2);
    }

    /**
     *
     */
    @Test
    public void testAdd_Wrapper() {
        out.println("add");
        Wrapper puntoCinetico = new Wrapper(new DimensionKinetic(1.0, 1.0), 0, true);
        KineticSortedList instance = new KineticSortedList();
        boolean expResult = true;
        boolean result = instance.add(puntoCinetico);
        assertEquals(expResult, result);
        assertTrue(instance.size() == 1);
    }

    /*    @Test
    public void testCheckValidity() {
        System.out.println("checkValidity");
        KineticSortedList instance = new KineticSortedList();
        instance.checkValidity();
    } */

    /**
     *
     */

    /*@Test
    public void testFindIds() {
        System.out.println("findIds");
        int objectId = 0;
        this.lista.add(new Wrapper(new DimensionKinetic(1.5, -2.0), 0, false));
        this.lista.add(new Wrapper(new DimensionKinetic(1.0, -1.0), 1, false));
        this.lista.add(new Wrapper(new DimensionKinetic(0.7, 0.0), 2, false));
        this.lista.add(new Wrapper(new DimensionKinetic(0.7, 1.0), 2, true));
        this.lista.add(new Wrapper(new DimensionKinetic(1.5, 2.0), 0, true));
        this.lista.add(new Wrapper(new DimensionKinetic(1.0, 3.0), 1, true));

        Pair<Integer> expResult = new PairInteger(0, 4);
        Pair<Integer> result = this.lista.findIds(objectId);
        assertEquals(expResult, result);
    }*/

    /**
     *
     */
    @Test
    public void testObtainCompleteListOfPairs() {
        out.println("obtainCompleteListOfPairs");
        Integer first = 2;
        Integer second = 4;

        this.lista.add(new Wrapper(new DimensionKinetic(1.5, -2.0), 0, false));
        this.lista.add(new Wrapper(new DimensionKinetic(1.0, -1.0), 1, false));
        this.lista.add(new Wrapper(new DimensionKinetic(0.7, 0.0), 2, false));
        this.lista.add(new Wrapper(new DimensionKinetic(0.7, 1.0), 2, true));
        this.lista.add(new Wrapper(new DimensionKinetic(1.5, 2.0), 0, true));
        this.lista.add(new Wrapper(new DimensionKinetic(1.0, 3.0), 1, true));

        List<Pair<Integer>> expResult = new ArrayList<>();
        expResult.add(new PairInteger(2, 0));

        List<Pair<Integer>> result = this.lista.obtainCompletePairsList(first, second);
        assertEquals(expResult, result);
    }

    /**
     * Test of sort method, of class KineticSortedList.
     */
    @Test
    public void testSort_2() {
        out.println("sort without need of delta");
        double time = 3;
        double delta = time/1000;
        this.lista.add(new Wrapper(new DimensionKinetic(1.5, -2.0), 0, false));
        this.lista.add(new Wrapper(new DimensionKinetic(1.0, -1.0), 1, false));
        this.lista.add(new Wrapper(new DimensionKinetic(0.7, 0.0), 2, false));
        this.lista.add(new Wrapper(new DimensionKinetic(0.7, 1.0), 2, true));
        this.lista.add(new Wrapper(new DimensionKinetic(1.5, 2.0), 0, true));
        this.lista.add(new Wrapper(new DimensionKinetic(1.0, 3.0), 1, true));
        KineticSortedList instance = this.lista;

        KineticSortedList expResult = new KineticSortedList();
        expResult.add(new Wrapper(new DimensionKinetic(1.0, -1.0), 1, false));
        expResult.add(new Wrapper(new DimensionKinetic(0.7, 0.0), 2, false));
        expResult.add(new Wrapper(new DimensionKinetic(1.5, -2.0), 0, false));
        expResult.add(new Wrapper(new DimensionKinetic(0.7, 1.0), 2, true));
        expResult.add(new Wrapper(new DimensionKinetic(1.0, 3.0), 1, true));
        expResult.add(new Wrapper(new DimensionKinetic(1.5, 2.0), 0, true));
        
        instance.sort(time,delta);
        assertEquals(expResult, instance);
    }

    /**
     * Test of sort method, of class KineticSortedList.
     */
    @Test
    public void testSort() {
        out.println("sort");
        double time = 0.246491049887176;
        double delta = 1E-7;
        double v1 = -0.314378159011376;
        double v2 = 9.359414005974480;
        double v3 = 3.674594350700790;
        double k1 = 1.76110092364815;
        double k2 = -0.6234990014112;
        double k3 = 0.777815012033019;
        
        DimensionKinetic d1 = new DimensionKinetic(v1, k1);
        DimensionKinetic d2 = new DimensionKinetic(v2, k2);
        DimensionKinetic d3 = new DimensionKinetic(v3, k3);
        
        KineticSortedList instance = new KineticSortedList();
        instance.add(new Wrapper(d1, 0, true));
        instance.add(new Wrapper(d2, 1, true));
        instance.add(new Wrapper(d3, 2, true));
        instance.showPositions(new PairInteger(0, 2), time);
        
        assertFalse(instance.certificateValidation(time));
        instance.sort(time,delta);
        assertTrue(instance.certificateValidation(time,delta));
    }
}
