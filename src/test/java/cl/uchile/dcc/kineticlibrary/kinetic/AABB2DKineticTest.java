package cl.uchile.dcc.kineticlibrary.kinetic;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import cl.uchile.dcc.staticlibrary.primitives.AABB2D;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class AABB2DKineticTest {

    List<Point2D> listaPuntos = new ArrayList<>();
    Point2D p, q;
    double vx, vy;
    AABB2DKinetic cajaMovil;

    /**
     *
     */
    public AABB2DKineticTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        this.p = new Point2D(1.75, 3.25);
        this.q = new Point2D(4.25, 6.75);
        this.listaPuntos.add(this.p);
        this.listaPuntos.add(this.q);
        this.vx = 3.0;
        this.vy = 1.0;
        this.cajaMovil = new AABB2DKinetic(this.listaPuntos, new PairDouble(this.vx, this.vy));
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.listaPuntos.clear();
    }

    /**
     *
     */
    @Test
    public void testGetVx() {
        out.println("getVx");
        AABB2DKinetic instance = this.cajaMovil;
        double expResult = 3.0;
        double result = instance.getVx();
        assertEquals(expResult, result, 0.0);
    }

    /**
     *
     */
    @Test
    public void testSetVx() {
        out.println("setVx");
        double v = 2.0;
        AABB2DKinetic instance = this.cajaMovil;
        instance.setVx(v);
        assertEquals(v, instance.getVx(), 0.0);
    }

    /**
     *
     */
    @Test
    public void testGetVy() {
        out.println("getVy");
        AABB2DKinetic instance = this.cajaMovil;
        double expResult = 1.0;
        double result = instance.getVy();
        assertEquals(expResult, result, 0.0);
    }

    /**
     *
     */
    @Test
    public void testSetVy() {
        out.println("setVy");
        double v = 2.0;
        AABB2DKinetic instance = this.cajaMovil;
        instance.setVy(v);
        assertEquals(v, instance.getVy(), 0.0);
    }

    /**
     *
     */
    @Test
    public void testGetBox() {
        out.println("getBox");
        double time = 0.0;
        AABB2DKinetic instance = this.cajaMovil;
        AABB2D expResult = new AABB2D(new Point2D(3, 5), 2.5, 3.5);
        AABB2D result = instance.getBox(time);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testGetInterval() {
        out.println("getInterval");
        int dimension0 = 0, dimension1 = 1;
        AABB2DKinetic instance = this.cajaMovil;
        Interval expResult0 = new Interval(new DimensionKinetic(3.0, 1.75), new DimensionKinetic(3.0, 4.25));
        Interval result0 = instance.getInterval(dimension0);
        Interval expResult1 = new Interval(new DimensionKinetic(1.0, 3.25), new DimensionKinetic(1.0, 6.75));
        Interval result1 = instance.getInterval(dimension1);
        assertEquals(expResult0.maximum, result0.maximum);
        assertEquals(expResult0.minimum, result0.minimum);
        assertEquals(expResult1.maximum, result1.maximum);
        assertEquals(expResult1.minimum, result1.minimum);
    }

    /**
     *
     */
    @Test
    public void testGetP() {
        out.println("getP");
        DimensionKinetic[] expResult = new DimensionKinetic[2];
        expResult[0] = new DimensionKinetic(3, 1.75);
        expResult[1] = new DimensionKinetic(1, 3.25);
        DimensionKinetic[] result = this.cajaMovil.getP();
        assertArrayEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testGetQ() {
        out.println("getQ");
        DimensionKinetic[] expResult = new DimensionKinetic[2];
        expResult[0] = new DimensionKinetic(3, 4.25);
        expResult[1] = new DimensionKinetic(1, 6.75);
        DimensionKinetic[] result = this.cajaMovil.getQ();
        assertArrayEquals(expResult, result);
    }
}
