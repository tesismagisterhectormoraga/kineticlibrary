/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.comparators;

import cl.uchile.dcc.kineticlibrary.kinetic.DimensionKinetic;
import cl.uchile.dcc.kineticlibrary.kinetic.Wrapper;
import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class WrapperComparatorTest {

    /**
     *
     */
    public WrapperComparatorTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testCompare() {
        out.println("compare");
        double time = 1.0;

        Wrapper i1 = new Wrapper(new DimensionKinetic(1.0, 0.0), 0, false);
        Wrapper i2 = new Wrapper(new DimensionKinetic(-1.0, 2.0), 1, true);
        Wrapper i3 = new Wrapper(new DimensionKinetic(1.1, 0.0), 2, true);

        WrapperComparator instance = new WrapperComparator(time);
        int expResult0 = 1, expResult1 = -1, expResult2 = -1;

        int result0 = instance.compare(i1, i2); // t(id=0, inicio) vs -t+2(id=1, fin) en t=1
        int result1 = instance.compare(i1, i3); // t(id=0, inicio) vs 1.1t(id=2, fin) en t=1
        int result2 = instance.compare(i2, i3); // -t+2(id=1, fin) vs 1.1t(id=2, fin) en t=1

        assertEquals(expResult0, result0); // 
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

}
