/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic;

import cl.uchile.dcc.kineticlibrary.kinetic.actuators.ActuatorsSuite;
import cl.uchile.dcc.kineticlibrary.kinetic.comparators.ComparatorsSuite;
import cl.uchile.dcc.kineticlibrary.kinetic.events.EventosSuite;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author hmoraga
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({Object2DTest.class, KineticSortedListTest.class, KSLListTest.class, Point2DKineticTest.class, IntervalTest.class, ActuatorsSuite.class, AABB2DKineticTest.class, DimensionKineticTest.class, IntersectionsListTest.class, WrapperTest.class, ComparatorsSuite.class, EventosSuite.class})
public class KineticSuite {

    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    @After
    public void tearDown() throws Exception {
    }

}
