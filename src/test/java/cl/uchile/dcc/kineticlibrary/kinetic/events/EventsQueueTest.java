/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.events;

import cl.uchile.dcc.kineticlibrary.kinetic.DimensionKinetic;
import cl.uchile.dcc.kineticlibrary.kinetic.IntersectionsList;
import cl.uchile.dcc.kineticlibrary.kinetic.KSLList;
import cl.uchile.dcc.kineticlibrary.kinetic.KineticSortedList;
import cl.uchile.dcc.kineticlibrary.kinetic.Object2D;
import cl.uchile.dcc.kineticlibrary.kinetic.Wrapper;
import cl.uchile.dcc.kineticlibrary.kinetic.actuators.Actuator;
import cl.uchile.dcc.kineticlibrary.kinetic.actuators.KineticStatistics;
import cl.uchile.dcc.staticlibrary.primitives.Pair;
import cl.uchile.dcc.staticlibrary.primitives.PairDouble;
import cl.uchile.dcc.staticlibrary.primitives.PairInteger;
import cl.uchile.dcc.staticlibrary.primitives.Point2D;
import static java.lang.System.out;
import java.util.ArrayList;
import java.util.List;
import java.util.Observer;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class EventsQueueTest {
    private KSLList KSL;
    private IntersectionsList listaColisiones;
    private KineticStatistics estadisticas;
    private DimensionKinetic px, qx, py, qy;
    private double time0, timeF;
    private List<Pair<Double>> dims;
    private EventsQueue cola;
    private EventsObserver observer;

    /**
     *
     */
    public EventsQueueTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        this.listaColisiones = new IntersectionsList();
        //colisiones = new Intersecciones();

        this.KSL = new KSLList(2);
        KineticSortedList kslx = new KineticSortedList();
        KineticSortedList ksly = new KineticSortedList();
        kslx.add(new Wrapper(new DimensionKinetic(1.0, -5.0), 0, false));
        kslx.add(new Wrapper(new DimensionKinetic(1.0, -1.0), 0, true));
        kslx.add(new Wrapper(new DimensionKinetic(-2.0, 1.0), 1, false));
        kslx.add(new Wrapper(new DimensionKinetic(-2.0, 3.0), 1, true));
        kslx.add(new Wrapper(new DimensionKinetic(-1.0, -3.0), 2, false));
        kslx.add(new Wrapper(new DimensionKinetic(-1.0, -1.0), 2, true));
        //kslx.sort(0.0, 0.00001);

        ksly.add(new Wrapper(new DimensionKinetic(-1.0, 1.0), 0, false));
        ksly.add(new Wrapper(new DimensionKinetic(-1.0, 2.0), 0, true));
        ksly.add(new Wrapper(new DimensionKinetic(0.0, -2.5), 1, false));
        ksly.add(new Wrapper(new DimensionKinetic(0.0, 1.5), 1, true));
        ksly.add(new Wrapper(new DimensionKinetic(2.0, -4.0), 2, false));
        ksly.add(new Wrapper(new DimensionKinetic(2.0, -2.0), 2, true));
        //ksly.sort(0.0, 0.0001);

        this.KSL.add(kslx);
        this.KSL.add(ksly);
        this.KSL.sort(this.time0);
        
        this.dims = new ArrayList<>();
        this.dims.add(new PairDouble(-10.0, 10.0));  // [-10,10] en X
        this.dims.add(new PairDouble(-10.0, 10.0));  // [-10,10] en Y
        this.time0 = 0.0;
        this.timeF = 10.0;

        this.estadisticas = new KineticStatistics();
        this.cola = new EventsQueue(new PairDouble(this.time0, this.timeF), false);

        this.observer = new EventsObserver(this.KSL, this.listaColisiones, this.cola, this.estadisticas, false);

        this.KSL.addObserver(this.observer);
        this.cola.addObserver(this.observer);
        this.listaColisiones.addObserver(this.observer);
    }

    /**
     *
     */
    @After
    public void tearDown() {
        this.KSL.clear();
    }

    /**
     *
     */
    @Test
    public void testCalcularEvents() {
        out.println("calcularEvents_Mixtos");
        EventsQueue instance = this.cola;
        instance.calculateInitialEvents();

        Event ev0 = instance.poll();
        assertTrue(new SwapEvent(4, 5, 0.5, 1).equals(ev0));
        Event ev1 = instance.poll();
        assertTrue(new SwapEvent(3, 4, 2.0 / 3.0, 0).equals(ev1));
        Event ev2 = instance.poll();
        assertTrue(new SwapEvent(0, 1, 0.75, 1).equals(ev2));
        Event ev3 = instance.poll();
        assertTrue(new SwapEvent(0, 1, 1.0, 0).equals(ev3));
        Event ev4 = instance.poll();
        assertTrue(new SwapEvent(2, 3, 1.0, 1).equals(ev4));
        assertTrue(instance.isEmpty());
    }

    /**
     *
     */
    @Test
    public void testCalculateInitialEvents() {
        out.println("calculateInitialEvents");
        this.cola.calculateInitialEvents();

        assertTrue(!this.cola.isEmpty());
    }

    /**
     *
     */
    @Test
    public void testCalcularEventsActuales() {
        out.println("calcularEventsActuales");
        boolean includeCurrentTime = true;
        assertTrue(this.cola.isEmpty());

        this.cola.calculateInitialEvents();
        assertFalse(this.cola.isEmpty());

        this.cola.setCurrentTime(1.0);
        this.cola.calculateCurrentEvents(includeCurrentTime);
        assertTrue(true);
    }

    /**
     *
     */
    @Test
    public void testGetTpo_inicial() {
        out.println("getTpo_inicial");
        double expResult = 0.0;
        double result = this.cola.getInitialTime();
        assertEquals(expResult, result, 0.0);
    }

    /**
     *
     */
    @Test
    public void testGetTpo_final() {
        out.println("getTpo_final");
        double expResult = 10.0;
        double result = this.cola.getFinalTime();
        assertEquals(expResult, result, 0.0);
    }

    /**
     *
     */
    @Test
    public void testGetCurrentTime() {
        out.println("getCurrentTime");
        double expResult = 0.0;
        double result = this.cola.getCurrentTime();
        assertEquals(expResult, result, 0.0);
    }

    /**
     *
     */
    @Test
    public void testSetCurrentTime() {
        out.println("setCurrentTime");
        double tpo_actual = 1.0;
        this.cola.setCurrentTime(tpo_actual);
        assertTrue(this.cola.getCurrentTime() == tpo_actual);
    }

    /**
     *
     */
    @Test
    public void testAdd() {
        out.println("add");
        Event e = new SwapEvent(0, 1, 5.3, 1);
        EventsQueue instance = this.cola;
        boolean expResult = true;
        boolean result = instance.add(e);
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testPeek() {
        out.println("peek");
        EventsQueue instance = this.cola;
        this.cola.calculateInitialEvents();

        Event expResult = new SwapEvent(4, 5, 0.5, 1);
        Event result = instance.peek();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testPoll() {
        out.println("poll");
        EventsQueue instance = this.cola;
        this.cola.calculateInitialEvents();

        Event expResult = new SwapEvent(4, 5, 0.5, 1);
        Event result = instance.poll();
        assertEquals(expResult, result);
    }

    /*@Test
    public void testExistenEventsMultiples() {
        System.out.println("existenEventsMultiples");
        cola.calcularEventsIniciales();
        boolean expResult = false;
        boolean result = cola.existenEventsMultiples(KSL);
        assertEquals(expResult, result);
    }*/

    /**
     *
     */

    
    @Test
    public void testSetObserver() {
        out.println("setObserver");
        Observer o = new EventsObserver(this.KSL, this.listaColisiones, this.cola, this.estadisticas, false);
        this.cola.setObserver(o);
    }

    /**
     *
     */
    @Test
    public void testAddAll() {
        out.println("addAll");
        List<SubEventSwap> masEvents = new ArrayList<>();
        masEvents.add(new SubEventSwap(3.5, 1, 4));
        int dimension = 0;
        EventsQueue instance = this.cola;
        this.cola.calculateInitialEvents();
        instance.addAll(masEvents, dimension);

        assertEquals(new SwapEvent(4, 5, 0.5, 1), this.cola.poll());
        assertEquals(new SwapEvent(3, 4, 2.0 / 3, 0), this.cola.poll());
        assertEquals(new SwapEvent(0, 1, 0.75, 1), this.cola.poll());
        assertEquals(new SwapEvent(0, 1, 1.0, 0), this.cola.poll());
        assertEquals(new SwapEvent(2, 3, 1.0, 1), this.cola.poll());
        assertEquals(new SwapEvent(1, 4, 3.5, 0), this.cola.poll());
        assertTrue(this.cola.isEmpty());
    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void testIsEmpty() throws Exception {
        out.println("isEmpty");

        assertTrue(this.cola.isEmpty());
        Actuator.structuresInitialization(this.cola, this.KSL, this.listaColisiones, this.estadisticas, true);
        this.cola.calculateInitialEvents();

        assertFalse(this.cola.isEmpty());
    }

    /**
     * Test of procesarEvents method, of class EventsQueue.
     * @throws java.lang.Exception
     */
    @Test
    public void testProcesarEvents() throws Exception {
        System.out.println("procesarEvents");
        Actuator.structuresInitialization(this.cola, this.KSL, this.listaColisiones, this.estadisticas, true);
        this.cola.processEvents();
        assertTrue(this.cola.isEmpty());
    }

    /**
     * Test of eliminarMultiplesEvents method, of class EventsQueue.
     * @throws java.lang.Exception
     */
    @Test
    public void testEliminarMultiplesEvents_falso() throws Exception {
        out.println("eliminarMultiplesEvents falso");
        Object2D o0, o1, o2;

        List<Point2D> listaPuntos0, listaPuntos1, listaPuntos2;
        Pair<Double> veloc0, veloc1, veloc2;

        listaPuntos0 = new ArrayList<>();
        listaPuntos0.add(new Point2D(-3, 1));
        listaPuntos0.add(new Point2D(-1, 1));
        listaPuntos0.add(new Point2D(-1, 3));
        listaPuntos0.add(new Point2D(-3, 3));

        veloc0 = new PairDouble(1.0, -1.0);

        o0 = new Object2D(0, listaPuntos0, veloc0);

        listaPuntos1 = new ArrayList<>();
        listaPuntos1.add(new Point2D(1, 2));
        listaPuntos1.add(new Point2D(4, 2));
        listaPuntos1.add(new Point2D(4, 4));
        listaPuntos1.add(new Point2D(1, 4));

        veloc1 = new PairDouble(-1.0, -2.0);

        o1 = new Object2D(1, listaPuntos1, veloc1);

        listaPuntos2 = new ArrayList<>();
        listaPuntos2.add(new Point2D(-2, -4));
        listaPuntos2.add(new Point2D(-1, -4));
        listaPuntos2.add(new Point2D(-1, -1));
        listaPuntos2.add(new Point2D(-2, -1));

        veloc2 = new PairDouble(1.0, 12.0);

        o2 = new Object2D(2, listaPuntos2, veloc2);

        KineticSortedList kslx = new KineticSortedList();
        KineticSortedList ksly = new KineticSortedList();

        kslx.add(o0, 0);
        kslx.add(o1, 0);
        kslx.add(o2, 0);

        ksly.add(o0, 1);
        ksly.add(o1, 1);
        ksly.add(o2, 1);

        //KSL.clear();
        this.KSL.add(kslx);
        this.KSL.add(ksly);

        Pair<Double> tiempos = new PairDouble(0.0, 2.0);

        this.dims = new ArrayList<>();
        this.dims.add(new PairDouble(-10.0, 10.0));  // [-10,10] en X
        this.dims.add(new PairDouble(-10.0, 10.0));  // [-10,10] en Y

        this.estadisticas = new KineticStatistics();
        this.cola = new EventsQueue(tiempos, false);

        this.observer = new EventsObserver(this.KSL, this.listaColisiones, this.cola, this.estadisticas, false);

        this.KSL.addObserver(this.observer);
        this.cola.addObserver(this.observer);
        this.listaColisiones.addObserver(this.observer);
        this.estadisticas.addObserver(this.observer);
        Actuator.structuresInitialization(this.cola, this.KSL, this.listaColisiones, this.estadisticas, true);
        
        boolean result = this.cola.existMultipleEvents();
        assertFalse(result);
    }

    /**
     * Test of eliminarMultiplesEvents method, of class EventsQueue.
     * @throws java.lang.Exception
     */
    @Test
    public void testEliminarMultiplesEvents_verdadero() throws Exception {
        out.println("eliminarMultiplesEvents verdadero");
        Object2D o0, o1, o2;

        List<Point2D> listaPuntos0, listaPuntos1, listaPuntos2;
        PairDouble veloc0, veloc1, veloc2;

        listaPuntos0 = new ArrayList<>();
        listaPuntos0.add(new Point2D(-2, -2));
        listaPuntos0.add(new Point2D(-2, 2));
        listaPuntos0.add(new Point2D(2, -2));

        veloc0 = new PairDouble(4.0, -3.0);

        o0 = new Object2D(0, listaPuntos0, veloc0);

        listaPuntos1 = new ArrayList<>();
        listaPuntos1.add(new Point2D(7, 4));
        listaPuntos1.add(new Point2D(7, 8));
        listaPuntos1.add(new Point2D(3, 8));

        veloc1 = new PairDouble(-1.0, -2.0);

        o1 = new Object2D(1, listaPuntos1, veloc1);

        listaPuntos2 = new ArrayList<>();
        listaPuntos2.add(new Point2D(1, -7));
        listaPuntos2.add(new Point2D(4, -7));
        listaPuntos2.add(new Point2D(2.5, -4));

        veloc2 = new PairDouble(1.0, 0.0);

        o2 = new Object2D(2, listaPuntos2, veloc2);

        KineticSortedList kslx = new KineticSortedList();
        KineticSortedList ksly = new KineticSortedList();

        kslx.add(o0, 0);
        kslx.add(o1, 0);
        kslx.add(o2, 0);

        ksly.add(o0, 1);
        ksly.add(o1, 1);
        ksly.add(o2, 1);

        this.KSL.clear();
        this.KSL.add(kslx);
        this.KSL.add(ksly);

        PairDouble tiempos = new PairDouble(0.0, 2.0);

        this.dims = new ArrayList<>();
        this.dims.add(new PairDouble(-10.0, 10.0));  // [-10,10] en X
        this.dims.add(new PairDouble(-10.0, 10.0));  // [-10,10] en Y

        this.estadisticas = new KineticStatistics();
        this.cola = new EventsQueue(tiempos, false);

        this.observer = new EventsObserver(this.KSL, this.listaColisiones, this.cola, this.estadisticas, false);

        this.KSL.addObserver(this.observer);
        this.cola.addObserver(this.observer);
        this.listaColisiones.addObserver(this.observer);
        this.estadisticas.addObserver(this.observer);
        Actuator.structuresInitialization(this.cola, this.KSL, this.listaColisiones, this.estadisticas, false);
        out.println("t=0.0");
        assertTrue(this.KSL.listsValidation(0.0));
        this.KSL.imprimir(0.0);
        out.println("t=0.2");
        Actuator.solveSimpleEvent(this.cola, this.KSL, this.listaColisiones, this.estadisticas, false); // t=1/5
        assertTrue(this.KSL.listsValidation(0.2));
        this.KSL.imprimir(0.2);
        out.println("t=2/3 en X");
        Actuator.solveSimpleEvent(this.cola, this.KSL, this.listaColisiones, this.estadisticas, false); // t=2/3 en X        
        assertFalse(this.KSL.listsValidation(2.0/3.0));  // falta elevento en Y en el mismo instante de tiempo
        out.println("t=2/3 en Y");
        Actuator.solveSimpleEvent(this.cola, this.KSL, this.listaColisiones, this.estadisticas, false); // t=2/3 en Y
        assertTrue(this.KSL.listsValidation(2.0/3.0));
        this.KSL.imprimir(2.0/3.0);
       
        boolean result = this.cola.existMultipleEvents();  // EV_{0,1,2}X(t=1)        
        assertTrue(result);
    }

    /**
     * Test of clear method, of class EventsQueue.
     */
    @Test
    public void testClear() {
        out.println("clear");
        EventsQueue instance = new EventsQueue(new PairDouble(0.0, 10.0), false);
        instance.add(new SwapEvent(0, 1, 3.5, 0));
        assertFalse(instance.isEmpty());
        instance.clear();
        assertTrue(instance.isEmpty());
    }

    /**
     * Test of existMultipleEvents method, of class EventsQueue.
     */
    @Test
    public void testExistMultipleEvents() {
        out.println("existMultipleEvents");
        EventsQueue instance0 = new EventsQueue(new PairDouble(1.0, 5.0), false);
        EventsQueue instance1 = new EventsQueue(new PairDouble(1.0, 5.0), false);
        
        Event ev0 = new SwapEvent(0, 1, 1.4, 0);
        Event ev1 = new SwapEvent(1, 2, 1.4, 0);
        Event ev2 = new SwapEvent(1, 2, 1.4, 1);
        
        instance0.add(ev0);
        instance0.add(ev1);
        
        instance1.add(ev0);
        instance1.add(ev2);
        
        boolean expResult0 = true;
        boolean expResult1 = false;
        
        boolean result0 = instance0.existMultipleEvents();
        boolean result1 = instance1.existMultipleEvents();

        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }

    /**
     * Test of obtenerRangoEliminar method, of class EventsQueue.
     */
    @Test
    public void testObtainRangeToDelete() {
        out.println("obtainRangeToDelete");
        double actualTime = 0.95;
        int dimension = 0;
        EventsQueue instance = new EventsQueue(new PairDouble(this.time0, this.timeF), true);

        instance.add(new SwapEvent(0, 1, 0.99, 1));
        instance.add(new SwapEvent(4, 5, 0.95, 0));
        instance.add(new SwapEvent(2, 3, 1.05, 1));
        instance.add(new SwapEvent(5, 6, 0.95, 1));
        instance.add(new SwapEvent(3, 4, 0.95, 0));
        instance.add(new SwapEvent(5, 6, 0.95, 0));
        instance.add(new SwapEvent(6, 7, 0.95, 0));
        
        instance.setCurrentTime(actualTime);
        
        Pair<Integer> expResult = new PairInteger(3, 7);
        Pair<Integer> result = instance.obtainRangeToDelete(actualTime, dimension);
        assertEquals(expResult, result);
    }
}
