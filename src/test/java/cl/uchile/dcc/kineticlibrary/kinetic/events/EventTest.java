/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.events;

import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class EventTest {

    /**
     *
     */
    public EventTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetTime() {
        out.println("getTime");
        Event instance2 = new SwapEvent(0, 2, 5.678, 0);
        double expResult2 = 5.678;
        double result2 = instance2.getTime();

        assertEquals(expResult2, result2, 0.0);
    }

    /**
     *
     */
    @Test
    public void testGetDimension() {
        out.println("getDimension");
        Event instance1 = new SwapEvent(1, 3, 2.75, 0);
        int expResult1 = 0;
        int result1 = instance1.getDimension();
        assertEquals(expResult1, result1);
    }

    /**
     *
     */
    @Test
    public void testSetTime() {
        out.println("setTime");
        double time = 1.0;
        Event instance = new SwapEvent(0, 1, 0.8, 1);
        instance.setTime(time);
        assertEquals(instance.getTime(), time, 0.0);
    }

    /**
     *
     */
    @Test
    public void testSetDimension() {
        out.println("setDimension");
        int dimension = 0;
        Event instance = new SwapEvent(0, 1, 0.8, 1);
        instance.setDimension(dimension);
        assertTrue(instance.getDimension() == dimension);
    }

    /**
     *
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        Event instance = new SwapEvent(0, 1, 0.8, 1);
        int expResult = 0;
        int result = instance.hashCode();
        assertFalse(expResult == result);
    }

    /**
     *
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj0 = new SwapEvent(0, 1, 0.8, 0);
        Object obj1 = new SwapEvent(0, 1, 0.8, 1);
        Object obj2 = new SwapEvent(0, 1, 0.7, 1);
        Event instance = new SwapEvent(0, 1, 0.8, 1);

        boolean expResult0 = false, expResult1 = true, expResult2 = false;
        boolean result0 = instance.equals(obj0);
        boolean result1 = instance.equals(obj1);
        boolean result2 = instance.equals(obj2);

        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
        assertEquals(expResult2, result2);
    }

    /**
     *
     */
    @Test
    public void testCompareTo() {
        out.println("compareTo");
        Event o = new SwapEvent(0, 1, 0.8, 1);
        Event instance0 = new SwapEvent(0, 1, 0.8, 0);
        Event instance1 = new SwapEvent(0, 1, 0.9, 1);

        int expResult0 = -1, expResult1 = 1;
        int result0 = instance0.compareTo(o);
        int result1 = instance1.compareTo(o);

        assertEquals(expResult0, result0);
        assertEquals(expResult1, result1);
    }
}
