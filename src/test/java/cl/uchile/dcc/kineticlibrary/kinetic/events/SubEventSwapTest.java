/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uchile.dcc.kineticlibrary.kinetic.events;

import static java.lang.System.out;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author hmoraga
 */
public class SubEventSwapTest {

    /**
     *
     */
    public SubEventSwapTest() {
    }

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     *
     */
    @Test
    public void testGetTime() {
        out.println("getTime");
        SubEventSwap instance = new SubEventSwap(1.5, 3, 4);
        double expResult = 1.5;
        double result = instance.getTime();
        assertEquals(expResult, result, 0.0);
    }

    /**
     *
     */
    @Test
    public void testSetTime() {
        out.println("setTime");
        double time = 3.6;
        SubEventSwap instance = new SubEventSwap(2.7, 1, 2);
        instance.setTime(time);
        assertEquals(instance.getTime(), time, 0.0);
    }

    /**
     *
     */
    @Test
    public void testGetId0() {
        out.println("getId0");
        SubEventSwap instance = new SubEventSwap(1.5, 3, 4);
        int expResult = 3;
        int result = instance.getId0();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testSetId0() {
        out.println("setId0");
        int id0 = 3;
        SubEventSwap instance = new SubEventSwap(1.5, 2, 4);
        instance.setId0(id0);
        assertTrue(instance.getId0() == id0);
    }

    /**
     *
     */
    @Test
    public void testGetId1() {
        out.println("getId1");
        SubEventSwap instance = new SubEventSwap(1.5, 3, 4);
        int expResult = 4;
        int result = instance.getId1();
        assertEquals(expResult, result);
    }

    /**
     *
     */
    @Test
    public void testSetId1() {
        out.println("setId1");
        int id1 = 3;
        SubEventSwap instance = new SubEventSwap(1.5, 2, 4);
        instance.setId1(id1);
        assertTrue(instance.getId1() == id1);
    }

    /**
     *
     */
    @Test
    public void testHashCode() {
        out.println("hashCode");
        SubEventSwap instance = new SubEventSwap(0.4566666, 2, 3);
        int expResult = 0;
        int result = instance.hashCode();
        assertTrue(expResult != result);
    }

    /**
     *
     */
    @Test
    public void testEquals() {
        out.println("equals");
        Object obj = new SubEventSwap(1.5, 4, 3);
        SubEventSwap instance = new SubEventSwap(1.5, 3, 4);
        boolean expResult = false;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
    }
}
